#Changelog

##v0.24.2 Balance changes

\* Boss attack speed halved

##v0.24.1 Fix collision bug

\* No longer crashes when people start dying.

##v0.24.0 GameCon Fixes

\+ Win condition and victory screen.

\* Fix bug where dead players recieve blood for living (and crash the game).

\* 2 player minimum reinstated.

##v0.23 Fix Various Bugs

\* Only the player that paused can use pause menu

\* Controllers now no longer control random player at times

##v0.22 Fix Boss

\+ Added boss health bar.

\* Waves don't spawn while boss is active.

\* Waves reset after a boss.

\* Boss can damage players, and players can damage boss.

\* Boss health decays over 30 seconds.

##v0.21 Add boss, Juice up heals

\+ Added boss (Transform for free rn, and transform back (both with right trigger) functions as a player still right now.)

\* Heal animation implemented.

\* Fixed attack hitbox going right while standing still regardless of direction.

##v0.20 Added menu-controller interaction, and various tune-ups

\+ Prompts now inform the player what to do on selection screen

\+ New methods in AudioPlayer wrapper

\* audioP in TestScene static (may want to change depending on how many SFX we have)

\* All buttons now sprites with child "selec" if highlighted

\* All "buttons" can be interacted with using the controller only

\* Pause menu screen pauses all animations and resumes them afterwards

\* Also pauses music and resumes after

\* When backgrounds change they now fade in and out

##v0.19.3 Added new separated button assets

\+ Play & Exit buttons without bloodsplat and separtae bloodsplat

##v0.19.2 Fixed death colors

\* boom colors now correct for each skin

##v0.19.1 Add Heal, Tidy death animations

\+ Players can heal by pressing Y costing 50% blood for 10% health

\* Death animations complete. Enemies fade out afterwards.

##v0.18 Fix Death Animations

\* Death animation code fixed

\+ additional commented out death code for other enemy types

##v0.17 Add blood

\+ Blood accumulates and is shown by the blood bar.

\+ Death animation code exists but doesn't work.

##v0.16 Various Tweaks and Changes

\+ Added extra checks in collisions

\* Skin selection works differently now

\* Updated vibrations to work with new controls

##v0.15 Update Attacks, Add stats

\* Attacks are less clunky

\* Stats exist, need proper implementation.

##v0.14.2 Update Player Skin Selection

\* Controls run through a loop of confirmed connected players

\* Minimum of 1 player to press play now

\* Player that presses start first, is first player, and so on

##v0.14.1 - Update Player Skin Selection

\* Only allows one instance of skin to be selected

\+ Minimum of 2 players to press play

##v0.13 - Add Player Skin Selection

\+ Player Select Screen

\+ Know which player controller is connected to

\* Player health bars display correct skin color

\* Title screen now leads to player select screen

##v0.12 - Add Four Players

\+ Four Players, Four controllers.

\+ Wave system (very unbalanced)


##v0.11 - Add Controller Support, Improve Ravager AI

\+ The two players are controlled by two controllers, or controller and keyboard if one controller is plugged in.

\+ Health bars use linear interpolation for a pretty effect.

\+ Ravager AI has dodgeable attacks coded in.

##v0.10 - Add Health Bars, Add 2nd Player

\+ Second player

\+ Health bar for each player

\+ Shadows under players and ravagers.

\* Ravager retargets to closest player.

##v0.9 - Add Audio Player

\+ Very basic Audio wrapper

\+ audio debugging commands

##v0.8 - Add basic AI

\+ Very basic AI

\* Room texture updated

##v0.7 - Add attacking, Add Ravager

\+ You can now damage by attacking left and right

\+ Ravager exists (is a punching bag atm)

\* Pillar and room textures updated

\+ Attacks animate

##v0.6 - Add attack animation

\+ Attacks animate

##v0.5 - Add pillar, Tweak menu button textures

\+ Pillars that you can walk behind

\* Menu buttons have seperate textures

##v0.4 - Add pause, Tweak main menu

\+ Pause feature. Pressing escape pauses physics engine,
    tints the screen, and brings up a menu.

\+ Pause layer frame (needs skinning) (top button resumes, bottom button quits)

\* Main menu has a quit button (still needs skinning) (same texture as other, bottom button quits)

##v0.3 - Add Menu Screen, Tweak player movement

\+ Menu Screen (One button, no background)

\* Player moves with extremely fast acceleration and deceleration.
  
##v0.2 - Add Depth

\+ Depth (You can walk behind enemies)
    (note: all sprites that use this should anchor at the bottom (0.0f))

##v0.1 - Initial Commit

\+ Character movement

\+ Rough Sprites

\+ Animation Wrapper

\+ Input and Display Manager

\+ Test Scene

\+ Linux Support

\+ Gitignore

\+ Changelog


*TODO: Animations and functional attack*