#ifndef __SWORDHIT_H__
#define __SWORDHIT_H__
#include "cocos2d.h"
#include "InputHandler.h"

USING_NS_CC;

class SwordHit {
public:
	SwordHit();
	Node* getSourceNode() { return sourceNode; }
	Node* getNode() { return node; }
	SwordHit(Scene *s, Vec2 pos, Size size, Node* source);
	~SwordHit();
	static int getCollisionTag() { return collisionTag; }
private:
	static int collisionTag;
	Node* node;
	Node* sourceNode;
};
#endif