#include "Enemy.h"
#include "EnemyHit.h"

//Collision tag for ALL enemies rn
int Enemy::collisionTag = 1;


//Constructors
Enemy::Enemy()
{
	attachedSprite = nullptr;
}

Enemy::Enemy(Scene* scene, Vec2 pos)
{
	stunned = 0.f;
	//Create sprite.
	attachedSprite = Sprite::create("test2.png");
	attachedSprite->setPosition(pos);
	attachedSprite->setAnchorPoint(Vec2(.5f, 0.f));
	//I want the explosions bigger so scaled sprites just for a bit
	attachedSprite->setScale(1.0f);
	scene->addChild(attachedSprite, 0);

	//Create a basic physics body for sprite and attach it.
	PhysicsBody* physicsBody = PhysicsBody::createBox(attachedSprite->getContentSize());
	physicsBody->setDynamic(true);
	physicsBody->setTag(collisionTag);
	physicsBody->setContactTestBitmask(0xFFFFFFFF);
	physicsBody->setRotationEnable(false);
	attachedSprite->setPhysicsBody(physicsBody);

	sceneHandle = scene;
}

//Destructor just sets the sprite to nullptr
Enemy::~Enemy()
{
	attachedSprite = nullptr;
}


//********************************************Methods********************************************//
bool Enemy::checkIfDead()
{
	if (health <= 0)
		return true;
	else
		return false;
}

void Enemy::takeDamage(int i, Vec2 source, float knockback)
{
	health-=i;
	stunned = 1.f;
	attachedSprite->getPhysicsBody()->applyImpulse((attachedSprite->getPosition() - source).getNormalized() * knockback);
	attachedSprite->runAction(TintTo::create(0.f,255, 127+126*health/3, 127+126*health/3));
}

void Enemy::kill()
{
	//will eventually need this prol
	//attachedSprite->runAction(RemoveSelf::create());
	attachedSprite->removeFromParentAndCleanup(1);
	delete this;
	//for now kill will do this
	//controller->runAnimation("death", MoveTo::create(0.0001f, Vec2(215, 215)));
	//health = 5;
}

void Enemy::cleanUpCallback(cocos2d::Ref* pSender)
{
	drawNode->clear();
	attachedSprite->removeFromParentAndCleanup(1);
	delete this;
}

void Enemy::update(float dt, std::vector<Player*>) {

}


//////////////////////////////////Ravager stuff from here//////////////////////////////////

//Constructors
Ravager::Ravager() : Enemy()
{
	attachedSprite = nullptr;
	drawNode = nullptr;
	targetNode = nullptr;
	controller = nullptr;
}

Ravager::Ravager(Scene* scene, Vec2 pos, Node* target) : Enemy()
{
	attack=10.f;
	targetNode = target;
	//set new stuffs
	health = 3;
	attack = 1;
	bloodOutput = 1;
	//Adds LESSERGHOUL folder path to ENEMIES folder path
	path += "RAVAGER/";

	//Create sprite.
	attachedSprite = Sprite::create(path + "ANIMATIONS/IdleRight/IdleRight1.png");
	attachedSprite->setAnchorPoint(Vec2(.5f, .1f));
	attachedSprite->setScale(.25f);
	attachedSprite->setPosition(pos);

	//Add a shadow
	auto shadow = Sprite::create("HERO/Shadow.png");
	shadow->setAnchorPoint(Vec2(.5f, 0.f));
	shadow->setPosition(480.f, 50.f);
	shadow->setScale(4.f);
	attachedSprite->addChild(shadow, -1);

	scene->addChild(attachedSprite, 0);

	//Create a basic physics body for sprite and attach it.
	Size box = attachedSprite->getContentSize();
	box.width = 350.f;
	box.height = 85.f;
	PhysicsBody* physicsBody = PhysicsBody::createBox(box);
	physicsBody->setPositionOffset(Vec2(0.f, -85.f));
	physicsBody->setDynamic(true);
	physicsBody->setTag(collisionTag);
	physicsBody->setContactTestBitmask(0xFFFFFFFF);
	physicsBody->setRotationEnable(false);
	physicsBody->setLinearDamping(2.f);

	physicsBody->setName("Ravager");
	attachedSprite->setName("Ravager");

	attachedSprite->setPhysicsBody(physicsBody);

	//Create your animations here
	controller = new AnimationController(attachedSprite);
	controller->addAnimation("IdleRight", path);
	controller->addAnimation("HopRight", path);
	controller->addAnimation("WalkRight", path, -1, 0.2);
	controller->addAnimation("AttackRight", path, -1, 0.1f);
	controller->addAnimation("Death", path, 1, 0.2f);
	controller->runAnimation("IdleRight");
	

	active = false;

	left = false;
	right = false;

	drawNode = DrawNode::create();
	scene->addChild(drawNode);
	sceneHandle = scene;
}

Ravager::~Ravager()
{
	delete controller;
	attachedSprite = nullptr;
	drawNode = nullptr;
	targetNode = nullptr;
}

//Eventually won't need but as of rn need :shrug:
void Ravager::kill()
{
	//will eventually need this prol
	//attachedSprite->stopAllActions();

	controller->runAnimation("Death");
	attachedSprite->runAction(Sequence::create(DelayTime::create(0.8f), FadeOut::create(0.7f), CallFuncN::create(CC_CALLBACK_1(Enemy::cleanUpCallback, this)), nullptr));
	
	//for now kill will do this
	//controller->runAnimation("death", MoveTo::create(0.0001f, Vec2(215, 215)));
}

void Ravager::update(float dt, std::vector<Player*> targets) {
	/*if (stunned > 0.f) {
		stunned -= dt;
		return;
	}*/
	//Pick the nearest target.
	{
		Node* bestNode = nullptr;
		float bestDist = 10000000000.f;
		for (int i = 0; i < targets.size(); i++) {
			if ((attachedSprite->getPosition() - targets[i]->getAttachedSprite()->getPosition()).length() < bestDist) {
				bestNode = targets[i]->getAttachedSprite();
				bestDist = (attachedSprite->getPosition() - targets[i]->getAttachedSprite()->getPosition()).length();
			}
		}
		targetNode = bestNode;
	}
	//The depth effect.
	attachedSprite->setZOrder(-(attachedSprite->getPosition().y));

	drawNode->clear();

	if (targetNode != nullptr) {
		//Attack Queue State
		if (attackqueue) {
			attackDelayTimer -= dt;
			chasing = false;
			attacking = false;
			if (attackDelayTimer <= 0) {
				controller->runAnimation("AttackRight");
				attackTimer = 0.4f;
				attackStampTimer = 0.2f;
				attackqueue = false;
				attacking = true;
				chasing = false;
			}
		}
		else
		//Attacking state
		if (attacking) {
			attackTimer -= dt;
			if (attackStampTimer > 0.f) {
				attackStampTimer -= dt;
				if (attackStampTimer <= 0.f) {
					//Stamp attack hitbox
					if (attachedSprite->isFlippedX())
						auto s = new EnemyHit(sceneHandle, Vec2(attachedSprite->getPosition().x - 75, attachedSprite->getPosition().y), Size(75, 48), 10);
					else
						auto s = new EnemyHit(sceneHandle, Vec2(attachedSprite->getPosition().x, attachedSprite->getPosition().y), Size(75, 48), 10);
				}
			}
			if (attackTimer <= 0.f) {
				//Finish attack.
				chasing = true;
				attacking = false;
				attackqueue = false;
				controller->runAnimation("WalkRight");
			}
		}
		else
		//Run state
		if (chasing) {
			//Targeting distance and direction vector
			Vec2 targetDir = targetNode->getPosition() - attachedSprite->getPosition();
			float targetDist = targetDir.length();
			targetDir.normalize();
			{
				attachedSprite->getPhysicsBody()->setVelocity(attachedSprite->getPhysicsBody()->getVelocity() + targetDir*512.f*dt*5.f);
				if (attachedSprite->getPhysicsBody()->getVelocity().length() > 480.f) {
					attachedSprite->getPhysicsBody()->setVelocity(attachedSprite->getPhysicsBody()->getVelocity().getNormalized() *= 480.f);
				}


				if (attachedSprite->getPhysicsBody()->getVelocity().x > 0 && targetDir.x > 0) {
					if (!right) {
						controller->runAnimation("WalkRight");
						attachedSprite->setFlippedX(false);
						right = true;
						left = false;
					}
				}
				else if (attachedSprite->getPhysicsBody()->getVelocity().x < 0 && targetDir.x < 0) {
					if (!left) {
						controller->runAnimation("WalkRight");
						attachedSprite->setFlippedX(true);
						right = false;
						left = true;
					}
				}
				if ((targetNode->getPosition() - attachedSprite->getPosition()).length() < 128.f) {
					//Start attack
					attackqueue = true;
					attacking = false;
					chasing = false;
					attackDelayTimer = 0.5f;
					controller->runAnimation("IdleRight");
				}

			}
		}
	}
	else {
		controller->runAnimation("IdleRight");
	}

}

void Ravager::hop() {

}
//////////////////////////////////Ghoul stuff from here//////////////////////////////////

//Constructors
//////////////////////////////////Ghoul stuff from here//////////////////////////////////

//Constructors
Ghoul::Ghoul() : Enemy()
{
	attachedSprite = nullptr;
	drawNode = nullptr;
	targetNode = nullptr;
	controller = nullptr;
}

Ghoul::Ghoul(Scene* scene, Vec2 pos, Node* target) : Enemy()
{
	attack=10.f;
	targetNode = target;
	//set new stuffs
	health = 1;
	attack = 1;
	bloodOutput = 1;
	//Adds LESSERGHOUL folder path to ENEMIES folder path
	path += "GHOUL/";


	//Create sprite.
	attachedSprite = Sprite::create(path + "ANIMATIONS/IdleRight/IdleRight1.png");
	attachedSprite->setAnchorPoint(Vec2(.5f, .0f));
	attachedSprite->setPosition(pos);

	//Add a shadow
	auto shadow = Sprite::create("HERO/Shadow.png");
	shadow->setAnchorPoint(Vec2(.5f, 0.f));
	shadow->setPosition(256.f, 0.f);
	attachedSprite->addChild(shadow, -1);

	attachedSprite->setScale(0.75f);
	scene->addChild(attachedSprite, 0);

	//Create a basic physics body for sprite and attach it.
	Size box = attachedSprite->getContentSize();
	box.width /= 6;
	box.height /= 10;
	PhysicsBody* physicsBody = PhysicsBody::createBox(box);
	physicsBody->setPositionOffset(Vec2(0.0f, box.height*.5f*.75f - attachedSprite->getContentSize().height*.5f*.75f));
	physicsBody->setDynamic(true);
	physicsBody->setTag(collisionTag);
	physicsBody->setContactTestBitmask(0xFFFFFFFF);
	physicsBody->setRotationEnable(false);
	physicsBody->setLinearDamping(2.f);

	physicsBody->setName("Ghoul");
	attachedSprite->setName("Ghoul");

	attachedSprite->setPhysicsBody(physicsBody);

	//Create your animations here
	controller = new AnimationController(attachedSprite);
	controller->addAnimation("IdleRight", path, -1, 0.4);
	controller->addAnimation("WalkRight", path, -1, 0.4);
	controller->addAnimation("AttackRight", path, -1, 0.4f);
	controller->addAnimation("Death", path, 1, 0.2f);
	controller->runAnimation("WalkRight");

	active = false;

	left = false;
	right = false;

	drawNode = DrawNode::create();
	scene->addChild(drawNode);
	sceneHandle = scene;
}

Ghoul::~Ghoul()
{
	delete controller;
	attachedSprite = nullptr;
	drawNode = nullptr;
	targetNode = nullptr;
}

//Eventually won't need but as of rn need :shrug:
void Ghoul::kill()
{
	//new Corpse(attachedSprite->getScene(), path, "Death", attachedSprite->getPosition(), attachedSprite->getScale(), 0.1f, attachedSprite->getZOrder());
	//will eventually need this prol
	//attachedSprite->stopAllActions();
	controller->runAnimation("Death");
	attachedSprite->runAction(Sequence::create(DelayTime::create(0.8f), FadeOut::create(0.7f), CallFuncN::create(CC_CALLBACK_1(Enemy::cleanUpCallback, this)),nullptr));
	//for now kill will do this
	//controller->runAnimation("death", MoveTo::create(0.0001f, Vec2(215, 215)));
}

void Ghoul::update(float dt, std::vector<Player*> targets) {

	//Pick the nearest target.
	{
		Node* bestNode = nullptr;
		float bestDist = 10000000000.f;
		for (int i = 0; i < targets.size(); i++) {
			if ((attachedSprite->getPosition() - targets[i]->getAttachedSprite()->getPosition()).length() < bestDist) {
				bestNode = targets[i]->getAttachedSprite();
				bestDist = (attachedSprite->getPosition() - targets[i]->getAttachedSprite()->getPosition()).length();
			}
		}
		targetNode = bestNode;
	}
	//The depth effect.
	attachedSprite->setZOrder(-(attachedSprite->getPosition().y));

	drawNode->clear();

	if (targetNode != nullptr) {
		//Attack Queue State
		if (attackqueue) {
			attackDelayTimer -= dt;
			chasing = false;
			attacking = false;
			if (attackDelayTimer <= 0) {
				controller->runAnimation("AttackRight");
				attackTimer = 0.8f;
				attackStampTimer = 0.4f;
				attackqueue = false;
				attacking = true;
				chasing = false;
			}
		}
		else
		//Attacking state
		if (attacking) {
			attackTimer -= dt;
			if (attackStampTimer > 0.f) {
				attackStampTimer -= dt;
				if (attackStampTimer <= 0.f) {
					//Stamp attack hitbox
					if (attachedSprite->isFlippedX())
						auto s = new EnemyHit(sceneHandle, Vec2(attachedSprite->getPosition().x - 75, attachedSprite->getPosition().y-18.f), Size(75, 64), 10);
					else
						auto s = new EnemyHit(sceneHandle, Vec2(attachedSprite->getPosition().x, attachedSprite->getPosition().y-18.f), Size(75, 64), 10);
				}
			}
			if (attackTimer <= 0.f) {
				//Finish attack.
				chasing = true;
				attacking = false;
				attackqueue = false;
				controller->runAnimation("WalkRight");
			}
		}
		else
		//Run state
		if (chasing) {
			//Targeting distance and direction vector
			Vec2 targetDir = targetNode->getPosition() - attachedSprite->getPosition();
			float targetDist = targetDir.length();
			targetDir.normalize();
			{
				attachedSprite->getPhysicsBody()->setVelocity(attachedSprite->getPhysicsBody()->getVelocity() + targetDir*512.f*dt*5.f);
				if (attachedSprite->getPhysicsBody()->getVelocity().length() > 64.f) {
					attachedSprite->getPhysicsBody()->setVelocity(attachedSprite->getPhysicsBody()->getVelocity().getNormalized() *= 64.f);
				}


				if (attachedSprite->getPhysicsBody()->getVelocity().x > 0 && targetDir.x > 0) {
					if (!right) {
						controller->runAnimation("WalkRight");
						attachedSprite->setFlippedX(false);
						right = true;
						left = false;
					}
				}
				else if (attachedSprite->getPhysicsBody()->getVelocity().x < 0 && targetDir.x < 0) {
					if (!left) {
						controller->runAnimation("WalkRight");
						attachedSprite->setFlippedX(true);
						right = false;
						left = true;
					}
				}
				if ((targetNode->getPosition() - attachedSprite->getPosition()).length() < 80.f) {
					//Start attack
					attackqueue = true;
					attacking = false;
					chasing = false;
					attackDelayTimer = 0.5f;
					controller->runAnimation("IdleRight");
				}

			}
		}
	}
	else {
		controller->runAnimation("IdleRight");
	}

}

//Corpse::Corpse() {
//	attachedSprite = nullptr;
//}
//
//Corpse::Corpse(Scene* scene, std::string path, std::string s, Vec2 pos, float scale, float speed, int zOrder) {
//	attachedSprite = Sprite::create(path+"/ANIMATIONS/"+s+"/"+s+"1.png");
//	attachedSprite->setPosition(pos);
//	attachedSprite->setScale(scale);
//	attachedSprite->setZOrder(zOrder);
//	scene->addChild(attachedSprite);
//	anim = new AnimationController(attachedSprite);
//	anim->addAnimation(s, path, 4*speed, speed);
//	anim->runAnimation(s, Sequence::createWithTwoActions(FadeOut::create(0.2f), RemoveSelf::create()));
//}
//
//Corpse::~Corpse() {
//	delete anim;
//	anim = nullptr;
//	attachedSprite = nullptr;
//}