#include "PauseMenuLayer.h"
#include "TestScene.h"
#include "SimpleAudioEngine.h"
#include "Shape.h"
#include "PlayerSelectScreen.h"
USING_NS_CC;

Scene* PauseMenuLayer::createPauseLayer(bool *pauseHook, int _controller)
{
    auto scene = PauseMenuLayer::create();
	scene->pauseHook = pauseHook;
	scene->controller = _controller;
	return scene;
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in SplashScreenScene.cpp\n");
}

// on "init" you need to initialize your instance
bool PauseMenuLayer::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//Tint background
	auto rect = new C_Rectangle(this, origin+0.5*(Vec2)visibleSize, visibleSize.width, visibleSize.height, Color4F(0.f,0.f,0.f,0.5f));

    //Add Label
	auto pauseLabel = Label::create("Paused", "fonts/Marker Felt.ttf", 64);
	pauseLabel->setAnchorPoint(Vec2(0.5f, 0.5f));
	pauseLabel->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2 + 120));
	addChild(pauseLabel);

	// add play button
	play = Sprite::create("UI/playbutton.png");
	play->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));
	
	// add blood splatter icon thing
	highlightButton = Sprite::create("UI/bloodsplat.png");
	highlightButton->setPosition(Vec2(165, 50));
	//avoids garbage collection
	highlightButton->retain();
	play->addChild(highlightButton, 1, "selec");
	this->addChild(play, 0);

	//add quit button
	back = Sprite::create("UI/quitbutton.png");
	back->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2 - 1));
	//quit->addChild(highlightButton, 1, "selec");
	this->addChild(back, 0);

	this->scheduleUpdate();
    return true;
}

void PauseMenuLayer::update(float dt) {
	if (*pauseHook == false) {
		removeFromParentAndCleanup(true);
		return;
	}
	XINPUT->DownloadPackets(4);
	//for (int i = 0; i < TestScene::controlOrder.size(); i++) {
		if (XINPUT->GetButton(controller, Button::DPadDown)) {
			if (back->getChildByName("selec") == nullptr) {
				play->removeChildByName("selec");
				/*highlightButton = Sprite::create("UI/bloodsplat.png");
				highlightButton->setPosition(Vec2(165, 50));*/
				back->addChild(highlightButton, 1, "selec");
			}
		}
		if (XINPUT->GetButton(controller, Button::DPadUp)) {
			if (play->getChildByName("selec") == nullptr) {
				back->removeChildByName("selec");
				/*highlightButton = Sprite::create("UI/bloodsplat.png");
				highlightButton->setPosition(Vec2(165, 50));*/
				play->addChild(highlightButton, 1, "selec");
			}
		}
		if (XINPUT->GetButton(controller, Button::A)) {
			if (play == nullptr) {
				return;
			}
			if (play->getChildByName("selec") == nullptr) {
				//makes sure that quit isn't a nullptr either
				if (back->getChildByName("selec") != nullptr) {
					back->runAction(CallFuncN::create(CC_CALLBACK_1(PauseMenuLayer::menuBackCallback, this)));
				}
			}
			else {
				play->runAction(CallFuncN::create(CC_CALLBACK_1(PauseMenuLayer::menuResumeCallback, this)));
				//break;
			}
		}
	//}
}

void PauseMenuLayer::menuResumeCallback(cocos2d::Ref* pSender/*, ui::Widget::TouchEventType type*/) {
	//Close layer off.
	(*pauseHook) = false;
	Director::getInstance()->getRunningScene()->getPhysicsWorld()->setSpeed(1.f);
}

void PauseMenuLayer::menuBackCallback(cocos2d::Ref* pSender/*, ui::Widget::TouchEventType type*/) {
	//Exit application.//Close the cocos2d-x game scene and quit the application
	TestScene::controlOrder.clear();
	for (int i = 0; i < 4; i++) {
		Player::skindex[i] = i;
	}
	Player::numberOfPlayers = 0;
	Director::getInstance()->replaceScene(PlayerSelectScreen::createScene());

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif

}
