#include "PlayerSelectScreen.h"
//#include "TestScene.h"
#include "SimpleAudioEngine.h"
#include "MenuScreen.h"

USING_NS_CC;

Scene* PlayerSelectScreen::createScene()
{
	return PlayerSelectScreen::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in SplashScreenScene.cpp\n");
}

// on "init" you need to initialize your instance
bool PlayerSelectScreen::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}
	visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	// add background
	auto background = Sprite::create("UI/PlayerSelectBackground.png");
	background->setAnchorPoint(Vec2(.0f, .0f));
	background->setPosition(origin);
	this->addChild(background);

	//Add Label
	prompt = Label::create("Press start or enter to join.", "fonts/Marker Felt.ttf", 30);
	prompt->setAnchorPoint(Vec2(0.5f, 0.5f));
	prompt->setPosition(Vec2(visibleSize.width / 2, 120));
	addChild(prompt, 1);
	prompt2 = Label::create("", "fonts/Marker Felt.ttf", 30);
	prompt2->setAnchorPoint(Vec2(0.5f, 0.5f));
	prompt2->setPosition(Vec2(visibleSize.width / 2, 60));
	addChild(prompt2, 1);
	

	// add play button
	play = Sprite::create("UI/playbutton.png");
	play->setPosition(Vec2(visibleSize.width - 200, 100));
	this->addChild(play, 0);
	// add blood splatter icon thing
	highlightButton = Sprite::create("UI/bloodsplat.png");
	highlightButton->setPosition(Vec2(165, 50));
	//avoids garbage collection
	highlightButton->retain();
	//play->addChild(highlightButton, 1, "selec");
	if (!TestScene::audioP.checkIfPlaying("background1")) {
		TestScene::audioP.playAudio("background1");
		TestScene::audioP.setVolume("background1", 0.3f);
	}

	//add quit button
	back = Sprite::create("UI/quitbutton.png");
	back->setPosition(Vec2(200, 100));
	back->addChild(highlightButton, 1, "selec");
	
	this->addChild(back, 0);

	paths.push_back("HERO/");
	paths.push_back("HERO_GREEN/");
	paths.push_back("HERO_YELLOw/");
	paths.push_back("HERO_BLUE/");

	this->scheduleUpdate();
	return true;
}

void PlayerSelectScreen::update(float dt) {
	if (TestScene::audioP.checkIfPlaying("background2")) {
		
		fadeOut = true;
		fadeIn = true;
	}
	if (fadeOut) {
		fadeOut = TestScene::audioP.fadeOut("background2");
	}
	if (fadeIn) {
		fadeIn = TestScene::audioP.fadeIn("background1", 4);
	}
	XINPUT->DownloadPackets(5);
	delay += dt;
	
	if (delay < 0.5) {
		XINPUT->DownloadPackets(5);
	}
	else {
		for (int i = 0; i < 4; i++) {
			if (XINPUT->GetConnected(i)) {
				if (XINPUT->GetButton(i, Button::Start) && !(controllersIn[i])) {
					//TestScene::players.push_back(new Player(this, Vec2(540, 440), TestScene::players.size()));
					if (std::find(TestScene::controlOrder.begin(), TestScene::controlOrder.end(), i) == TestScene::controlOrder.end()) {
						prompt->setString("Use left and right on dpad to change skins and 'X' to confirm");
						TestScene::controlOrder.push_back(i);
						controllersIn[i] = true;
						playersIn[TestScene::controlOrder.size() - 1] = true;
						sprites[TestScene::controlOrder.size() - 1] = Sprite::create(paths[Player::skindex[TestScene::controlOrder.size() - 1]] + "ANIMATIONS/IdleRight/IdleRight1.png");
						sprites[TestScene::controlOrder.size() - 1]->setPosition(playerPos[TestScene::controlOrder.size() - 1]);
						sprites[TestScene::controlOrder.size() - 1]->setScale(2.0f);
						this->addChild(sprites[TestScene::controlOrder.size() - 1]);
						Player::numberOfPlayers++;
					}
				}
			}
		}
		for (int i = 0; i < TestScene::controlOrder.size(); i++) {
			if (XINPUT->GetConnected(TestScene::controlOrder[i])) {
				if (controllersIn[TestScene::controlOrder[i]] && !(readyPlayer[i])) {
					if (XINPUT->GetButton(TestScene::controlOrder[i], Button::DPadLeft)) {
						if (!(lastLeftState[i])) {
							lastLeftState[i] = true;
							if (Player::skindex[i] == 0) {
								Player::skindex[i] = maxAmount;
								checkSkindex(i, "-");
								sprites[i]->setTexture(paths[Player::skindex[i]] + "ANIMATIONS/IdleRight/IdleRight1.png");
							}
							else {
								Player::skindex[i] -= 1;
								checkSkindex(i, "-");
								sprites[i]->setTexture(paths[Player::skindex[i]] + "ANIMATIONS/IdleRight/IdleRight1.png");
							}
						}
					}
					else {
						lastLeftState[i] = false;
					}
					if (XINPUT->GetButton(TestScene::controlOrder[i], Button::DPadRight)) {
						if (!(lastRightState[i])) {
							lastRightState[i] = true;
							if (Player::skindex[i] == maxAmount) {
								Player::skindex[i] = 0;
								checkSkindex(i, "+");
								sprites[i]->setTexture(paths[Player::skindex[i]] + "ANIMATIONS/IdleRight/IdleRight1.png");
							}
							else {
								Player::skindex[i] += 1;
								checkSkindex(i, "+");
								sprites[i]->setTexture(paths[Player::skindex[i]] + "ANIMATIONS/IdleRight/IdleRight1.png");
							}
						}
					}
					else {
						lastRightState[i] = false;
					}
				}
			}
			if (XINPUT->GetButton(TestScene::controlOrder[i], Button::X) && !(readyPlayer[i])) {
				readyPlayer[i] = true;
				//paths.erase(paths.begin() + Player::skindex[i]);
				for (int j = 0; j < 4; j++) {
					if (Player::skindex[j] == Player::skindex[i]) {
						if (!(j == i) && playersIn[j]) {
							if (Player::skindex[j] != 0) {
								Player::skindex[j]--;
								sprites[j]->setTexture(paths[Player::skindex[j]] + "ANIMATIONS/IdleRight/IdleRight1.png");
							}
							else {
								Player::skindex[j] = maxAmount;
								sprites[j]->setTexture(paths[Player::skindex[j]] + "ANIMATIONS/IdleRight/IdleRight1.png");
							}
						}
					}
				}
				auto label = Label::create("READY", "fonts/arial.ttf", 24);
				label->setPosition(playerPos[i]);
				this->addChild(label);
			}
		}
		{
			int count = 0;
			for (int i = 0; i < 4; i++) {
				if (!controllersIn[i]) {
					count++;
				}
			}
			if (count == 4) {
				if (back->getChildByName("selec") == nullptr && (!enableBack)) { back->addChild(highlightButton, 1, "selec"); }
				enableBack = true;
			}
			else {
				enableBack = false;
			}
		}
		if (enableBack) {
			for (int i = 0; i < 4; i++) {
				if (XINPUT->GetButton(i, Button::B)) {
					back->runAction(CallFuncN::create(CC_CALLBACK_1(PlayerSelectScreen::menuBackCallback, this)));
				}
			}
		}
		for (int i = 0; i < TestScene::controlOrder.size(); i++) {
			if (controllersIn[TestScene::controlOrder[i]]) {
				if (readyPlayer[i]) {
					if (play->getChildByName("selec") == nullptr && (!enablePlay)) { 
						if (back->getChildByName("selec") != nullptr) {
							back->removeChildByName("selec");
						}
						play->addChild(highlightButton, 1, "selec"); 
					}
					prompt->setString("");
					enablePlay = true;
					//enableBack = true;
				}
				else {
					if (play->getChildByName("selec") != nullptr) { play->removeChildByName("selec"); }
					else if (back->getChildByName("selec") != nullptr) { back->removeChildByName("selec"); }
					prompt->setString("Use left and right on dpad to change skins and 'X' to confirm");
					enablePlay = false;
					//enableBack = false;
					break;
				}
			}
		}
		for (int i = 0; i < 4; i++) {
			//checks if one player has confirmed but more haven't
			if (controllersIn[i] && readyPlayer[i]) {
				bothTrue++;
			}
			if (controllersIn[i] && !readyPlayer[i]) {
				oneTrue++;
			}
		}
		for (int i = 0; i < 4; i++) {
			if (enablePlay) {
				if (XINPUT->GetButton(i, Button::DPadLeft)) {
					if (back->getChildByName("selec") == nullptr) {
						play->removeChildByName("selec");
						/*highlightButton = Sprite::create("UI/bloodsplat.png");
						highlightButton->setPosition(Vec2(165, 50));*/
						back->addChild(highlightButton, 1, "selec");
					}
				}
				if (XINPUT->GetButton(i, Button::DPadRight)) {
					if (play->getChildByName("selec") == nullptr) {
						back->removeChildByName("selec");
						/*highlightButton = Sprite::create("UI/bloodsplat.png");
						highlightButton->setPosition(Vec2(165, 50));*/
						play->addChild(highlightButton, 1, "selec");
					}
				}
			}
			if (XINPUT->GetButton(i, Button::A)) {
				if (play->getChildByName("selec") == nullptr) {
					//makes sure that quit isn't a nullptr either
					if (back->getChildByName("selec") != nullptr) {
						back->runAction(CallFuncN::create(CC_CALLBACK_1(PlayerSelectScreen::menuBackCallback, this)));
					}
				}
				else {
					play->runAction(CallFuncN::create(CC_CALLBACK_1(PlayerSelectScreen::menuPlayCallback, this)));
				}
			}
		}
	}
}

/*void PlayerSelectScreen::update(float dt) {
	XINPUT->DownloadPackets(4);
	if (XINPUT->GetConnected(0)) {
		if (XINPUT->GetButton(0, Button::Start) && !(playerIn[0])) {
			lastConfirmState[0] = true;
			playerIn[0] = true;
			sprites[0] = Sprite::create(paths[Player::skindex[0]] +"ANIMATIONS/IdleRight/IdleRight1.png");
			sprites[0]->setPosition(Vec2(200,800));
			sprites[0]->setScale(2.0f);
			this->addChild(sprites[0]);
			Player::numberOfPlayers++;
		}
		if (playerIn[0] && !(readyPlayer[0])) {
			if (XINPUT->GetButton(0, Button::DPadLeft)) {
				if (!(lastLeftState[0])) {
					lastLeftState[0] = true;
					if (Player::skindex[0] == 0) {
						Player::skindex[0] = maxAmount;
						sprites[0]->removeFromParentAndCleanup(true);
						sprites[0]->create(paths[Player::skindex[0]] + "ANIMATIONS/IdleRight/IdleRight1.png");
						sprites[0]->setPosition(playerPos[0]);
						sprites[0]->setScale(2.0f);
						this->addChild(sprites[0]);
					}
					else {
						Player::skindex[0] -= 1;
						sprites[0]->removeFromParentAndCleanup(true);
						sprites[0]->create(paths[Player::skindex[0]] + "ANIMATIONS/IdleRight/IdleRight1.png");
						sprites[0]->setPosition(playerPos[0]);
						sprites[0]->setScale(2.0f);
						this->addChild(sprites[0]);
					}
				}
			}
			else {
				lastLeftState[0] = false;
			}
			if (XINPUT->GetButton(0, Button::DPadRight)) {
				if (!(lastRightState[0])) {
					lastRightState[0] = true;
					if (Player::skindex[0] == maxAmount) {
						Player::skindex[0] = 0;
						sprites[0]->removeFromParentAndCleanup(true);
						sprites[0]->create(paths[Player::skindex[0]] + "ANIMATIONS/IdleRight/IdleRight1.png");
						sprites[0]->setPosition(playerPos[0]);
						sprites[0]->setScale(2.0f);
						this->addChild(sprites[0]);
					}
					else {
						Player::skindex[0] += 1;
						sprites[0]->removeFromParentAndCleanup(true);
						sprites[0]->create(paths[Player::skindex[0]] + "ANIMATIONS/IdleRight/IdleRight1.png");
						sprites[0]->setPosition(playerPos[0]);
						sprites[0]->setScale(2.0f);
						this->addChild(sprites[0]);
					}
				}
			}
			else {
				lastRightState[0] = false;
			}
			if (XINPUT->GetButton(0, Button::A) && !(readyPlayer[0])) {
				readyPlayer[0] = true;
				paths.erase(paths.begin() + Player::skindex[0]);
				maxAmount--;
				for (int i = 0; i < 4; i++) {
					if (Player::skindex[i] == Player::skindex[0]) {
						if (!(i == 0)&&playerIn[i]) {
							if (Player::skindex[i] != 0) {
								Player::skindex[i]--;
								sprites[i]->removeFromParentAndCleanup(true);
								sprites[i]->create(paths[Player::skindex[i]] + "ANIMATIONS/IdleRight/IdleRight1.png");
								sprites[i]->setPosition(playerPos[i]);
								sprites[i]->setScale(2.0f);
								this->addChild(sprites[i]);
							}
							else {
								Player::skindex[i] = maxAmount;
								sprites[i]->removeFromParentAndCleanup(true);
								sprites[i]->create(paths[Player::skindex[i]] + "ANIMATIONS/IdleRight/IdleRight1.png");
								sprites[i]->setPosition(playerPos[i]);
								sprites[i]->setScale(2.0f);
								this->addChild(sprites[i]);
							}
						}
					}
				}
				auto label = Label::create("READY", "fonts/arial.ttf", 24);
				label->setPosition(playerPos[0]);
				this->addChild(label);
			}
		}
	}
	if (XINPUT->GetConnected(1)) {
		if (XINPUT->GetButton(1, Button::Start) && !(playerIn[1])) {
			playerIn[1] = true;
			sprites[1] = Sprite::create(paths[Player::skindex[1]] + "ANIMATIONS/IdleRight/IdleRight1.png");
			sprites[1]->setPosition(Vec2(670, 800));
			sprites[1]->setScale(2.0f);
			this->addChild(sprites[1]);
			Player::numberOfPlayers++;
		}
		if (playerIn[1] && !(readyPlayer[1])) {
			if (XINPUT->GetButton(1, Button::DPadLeft)) {
				if (!(lastLeftState[1])) {
					lastLeftState[1] = true;
					if (Player::skindex[1] == 0) {
						Player::skindex[1] = maxAmount;
						sprites[1]->removeFromParentAndCleanup(true);
						sprites[1]->create(paths[Player::skindex[1]] + "ANIMATIONS/IdleRight/IdleRight1.png");
						sprites[1]->setPosition(playerPos[1]);
						sprites[1]->setScale(2.0f);
						this->addChild(sprites[1]);
					}
					else {
						Player::skindex[1] -= 1;
						sprites[1]->removeFromParentAndCleanup(true);
						sprites[1]->create(paths[Player::skindex[1]] + "ANIMATIONS/IdleRight/IdleRight1.png");
						sprites[1]->setPosition(playerPos[1]);
						sprites[1]->setScale(2.0f);
						this->addChild(sprites[1]);
					}
				}
			}
			else {
				lastLeftState[1] = false;
			}
			if (XINPUT->GetButton(1, Button::DPadRight)) {
				if (!(lastRightState[1])) {
					lastRightState[1] = true;
					if (Player::skindex[1] == maxAmount) {
						Player::skindex[1] = 0;
						sprites[1]->removeFromParentAndCleanup(true);
						sprites[1]->create(paths[Player::skindex[1]] + "ANIMATIONS/IdleRight/IdleRight1.png");
						sprites[1]->setPosition(playerPos[1]);
						sprites[1]->setScale(2.0f);
						this->addChild(sprites[1]);
					}
					else {
						Player::skindex[1] += 1;
						sprites[1]->removeFromParentAndCleanup(true);
						sprites[1]->create(paths[Player::skindex[1]] + "ANIMATIONS/IdleRight/IdleRight1.png");
						sprites[1]->setPosition(playerPos[1]);
						sprites[1]->setScale(2.0f);
						this->addChild(sprites[1]);
					}
				}
			}
			else {
				lastRightState[1] = false;
			}
			if (XINPUT->GetButton(1, Button::A) && !(readyPlayer[1])) {
				readyPlayer[1] = true;
				paths.erase(paths.begin() + Player::skindex[1]);
				maxAmount--;
				for (int i = 0; i < 4; i++) {
					if (Player::skindex[i] == Player::skindex[1]) {
						if (!(i == 1) && playerIn[i]) {
							if (Player::skindex[i] != 0) {
								Player::skindex[i]--;
								sprites[i]->removeFromParentAndCleanup(true);
								sprites[i]->create(paths[Player::skindex[i]] + "ANIMATIONS/IdleRight/IdleRight1.png");
								sprites[i]->setPosition(playerPos[i]);
								sprites[i]->setScale(2.0f);
								this->addChild(sprites[i]);
							}
							else {
								Player::skindex[i] = maxAmount;
								sprites[i]->removeFromParentAndCleanup(true);
								sprites[i]->create(paths[Player::skindex[i]] + "ANIMATIONS/IdleRight/IdleRight1.png");
								sprites[i]->setPosition(playerPos[i]);
								sprites[i]->setScale(2.0f);
								this->addChild(sprites[i]);
							}
						}
					}
				}
				auto label = Label::create("READY", "fonts/arial.ttf", 24);
				label->setPosition(playerPos[1]);
				this->addChild(label);
			}
		}
	}
	if (XINPUT->GetConnected(2)) {
		if (XINPUT->GetButton(2, Button::Start) && !(playerIn[2])) {
			playerIn[2] = true;
			sprites[2] = Sprite::create(paths[Player::skindex[2]] + "ANIMATIONS/IdleRight/IdleRight1.png");
			sprites[2]->setPosition(Vec2(1190, 800));
			sprites[2]->setScale(2.0f);
			this->addChild(sprites[2]);
			Player::numberOfPlayers++;
		}
		if (playerIn[2] && !(readyPlayer[2])) {
			if (XINPUT->GetButton(2, Button::DPadLeft)) {
				if (!(lastLeftState[2])) {
					lastLeftState[2] = true;
					if (Player::skindex[2] == 0) {
						Player::skindex[2] = maxAmount;
						sprites[2]->removeFromParentAndCleanup(true);
						sprites[2]->create(paths[Player::skindex[2]] + "ANIMATIONS/IdleRight/IdleRight1.png");
						sprites[2]->setPosition(playerPos[2]);
						sprites[2]->setScale(2.0f);
						this->addChild(sprites[2]);
					}
					else {
						Player::skindex[2] -= 1;
						sprites[2]->removeFromParentAndCleanup(true);
						sprites[2]->create(paths[Player::skindex[2]] + "ANIMATIONS/IdleRight/IdleRight1.png");
						sprites[2]->setPosition(playerPos[2]);
						sprites[2]->setScale(2.0f);
						this->addChild(sprites[2]);
					}
				}
			}
			else {
				lastLeftState[2] = false;
			}
			if (XINPUT->GetButton(2, Button::DPadRight)) {
				if (!(lastRightState[2])) {
					lastRightState[2] = true;
					if (Player::skindex[2] == maxAmount) {
						Player::skindex[2] = 0;
						sprites[2]->removeFromParentAndCleanup(true);
						sprites[2]->create(paths[Player::skindex[2]] + "ANIMATIONS/IdleRight/IdleRight1.png");
						sprites[2]->setPosition(playerPos[2]);
						sprites[2]->setScale(2.0f);
						this->addChild(sprites[2]);
					}
					else {
						Player::skindex[2] += 1;
						sprites[2]->removeFromParentAndCleanup(true);
						sprites[2]->create(paths[Player::skindex[2]] + "ANIMATIONS/IdleRight/IdleRight1.png");
						sprites[2]->setPosition(playerPos[2]);
						sprites[2]->setScale(2.0f);
						this->addChild(sprites[2]);
					}
				}
			}
			else {
				lastRightState[2] = false;
			}
			if (XINPUT->GetButton(2, Button::A) && !(readyPlayer[2])) {
				readyPlayer[2] = true;
				paths.erase(paths.begin() + Player::skindex[2]);
				maxAmount--;
				for (int i = 0; i < 4; i++) {
					if (Player::skindex[i] == Player::skindex[2]) {
						if (!(i == 2) && playerIn[i]) {
							if (Player::skindex[i] != 0) {
								Player::skindex[i]--;
								sprites[i]->removeFromParentAndCleanup(true);
								sprites[i]->create(paths[Player::skindex[i]] + "ANIMATIONS/IdleRight/IdleRight1.png");
								sprites[i]->setPosition(playerPos[i]);
								sprites[i]->setScale(2.0f);
								this->addChild(sprites[i]);
							}
							else {
								Player::skindex[i] = maxAmount;
								sprites[i]->removeFromParentAndCleanup(true);
								sprites[i]->create(paths[Player::skindex[i]] + "ANIMATIONS/IdleRight/IdleRight1.png");
								sprites[i]->setPosition(playerPos[i]);
								sprites[i]->setScale(2.0f);
								this->addChild(sprites[i]);
							}
						}
					}
				}
				auto label = Label::create("READY", "fonts/arial.ttf", 24);
				label->setPosition(playerPos[2]);
				this->addChild(label);
			}
		}
	}
	if (XINPUT->GetConnected(3)) {
		if (XINPUT->GetButton(3, Button::Start) && !(playerIn[3])) {
			playerIn[3] = true;
			sprites[3] = Sprite::create(paths[Player::skindex[3]] + "ANIMATIONS/IdleRight/IdleRight1.png");
			sprites[3]->setPosition(Vec2(1700, 800));
			sprites[3]->setScale(2.0f);
			this->addChild(sprites[3]);
			Player::numberOfPlayers++;
		}
		if (playerIn[3] && !(readyPlayer[3])) {
			if (XINPUT->GetButton(3, Button::DPadLeft)) {
				if (!(lastLeftState[3])) {
					lastLeftState[3] = true;
					if (Player::skindex[3] == 0) {
						Player::skindex[3] = maxAmount;
						sprites[3]->removeFromParentAndCleanup(true);
						sprites[3]->create(paths[Player::skindex[3]] + "ANIMATIONS/IdleRight/IdleRight1.png");
						sprites[3]->setPosition(playerPos[3]);
						sprites[3]->setScale(2.0f);
						this->addChild(sprites[3]);
					}
					else {
						Player::skindex[3] -= 1;
						sprites[3]->removeFromParentAndCleanup(true);
						sprites[3]->create(paths[Player::skindex[3]] + "ANIMATIONS/IdleRight/IdleRight1.png");
						sprites[3]->setPosition(playerPos[3]);
						sprites[3]->setScale(2.0f);
						this->addChild(sprites[3]);
					}
				}
			}
			else {
				lastLeftState[3] = false;
			}
			if (XINPUT->GetButton(3, Button::DPadRight)) {
				if (!(lastRightState[3])) {
					lastRightState[3] = true;
					if (Player::skindex[3] == maxAmount) {
						Player::skindex[3] = 0;
						sprites[3]->removeFromParentAndCleanup(true);
						sprites[3]->create(paths[Player::skindex[3]] + "ANIMATIONS/IdleRight/IdleRight1.png");
						sprites[3]->setPosition(playerPos[3]);
						sprites[3]->setScale(2.0f);
						this->addChild(sprites[3]);
					}
					else {
						Player::skindex[3] += 1;
						sprites[3]->removeFromParentAndCleanup(true);
						sprites[3]->create(paths[Player::skindex[3]] + "ANIMATIONS/IdleRight/IdleRight1.png");
						sprites[3]->setPosition(playerPos[3]);
						sprites[3]->setScale(2.0f);
						this->addChild(sprites[3]);
					}
				}
			}
			else {
				lastRightState[3] = false;
			}
			if (XINPUT->GetButton(3, Button::A) && !(readyPlayer[3])) {
				readyPlayer[3] = true;
				paths.erase(paths.begin() + Player::skindex[3]);
				maxAmount--;
				for (int i = 0; i < 4; i++) {
					if (Player::skindex[i] == Player::skindex[3]) {
						if (!(i == 3)&& playerIn[i]) {
							if (Player::skindex[i] != 0) {
								Player::skindex[i]--;
								sprites[i]->removeFromParentAndCleanup(true);
								sprites[i]->create(paths[Player::skindex[i]] + "ANIMATIONS/IdleRight/IdleRight1.png");
								sprites[i]->setPosition(playerPos[i]);
								sprites[i]->setScale(2.0f);
								this->addChild(sprites[i]);
							}
							else {
								Player::skindex[i] = maxAmount;
								sprites[i]->removeFromParentAndCleanup(true);
								sprites[i]->create(paths[Player::skindex[i]] + "ANIMATIONS/IdleRight/IdleRight1.png");
								sprites[i]->setPosition(playerPos[i]);
								sprites[i]->setScale(2.0f);
								this->addChild(sprites[i]);
							}
						}
					}
				}
				auto label = Label::create("READY", "fonts/arial.ttf", 24);
				label->setPosition(playerPos[3]);
				this->addChild(label);
			}
		}
	}
	if (Player::numberOfPlayers > 1) {
		for (int i = 0; i < 4; i++) {
			if (playerIn[i]) {
				if (readyPlayer[i]) {
					enablePlay = true;
				}
				else {
					enablePlay = false;
					break;
				}
			}
		}
	}
	if (enablePlay) {
		play->getButton()->setEnabled(true);
	}
}
*/
void PlayerSelectScreen::menuBackCallback(Ref* pSender)
{
	//Close the cocos2d-x game scene and quit the application
	TestScene::controlOrder.clear();
	for (int i = 0; i < 4; i++) {
		Player::skindex[i] = i;
	}
	Player::numberOfPlayers = 0;
	Director::getInstance()->replaceScene(MenuScreen::createScene());

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif

	/*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/

	//EventCustom customEndEvent("game_scene_close_event");
	//_eventDispatcher->dispatchEvent(&customEndEvent);


}

void PlayerSelectScreen::menuPlayCallback(cocos2d::Ref * pSender/*, ui::Widget::TouchEventType type*/)
{
	//if (type == ui::Widget::TouchEventType::ENDED)
	if (Player::numberOfPlayers>1)
		Director::getInstance()->replaceScene(TestScene::createScene());
}

void PlayerSelectScreen::checkSkindex(int i, std::string sign)
{
	bool neverOverlap = false;
	int overlaps = 0;
	while (!neverOverlap) {
		for (int j = 0; j < 4; j++) {
			if (readyPlayer[j]) {
				if (Player::skindex[i] == 0) {
					if (Player::skindex[j] == Player::skindex[i]) {
						if (sign == "+") {
							Player::skindex[i] += 1;
						}
						else {
							Player::skindex[i] = maxAmount;
						}
						overlaps++;
						break;
					}
				}
				else {
					if (Player::skindex[j] == Player::skindex[i]) {
						
						if (sign == "-") {
							Player::skindex[i] -= 1;
						}
						else if (sign == "+") {
							if (Player::skindex[i] == maxAmount) {
								Player::skindex[i] = 0;
							}
							else {
								Player::skindex[i] += 1;
							}
						}
						overlaps++;
						break;
					}
				}
			}
		}
		if (overlaps > 0) {
			overlaps = 0;
			neverOverlap = false;
		}
		else {
			overlaps = 0;
			neverOverlap = true;
		}
	}
}
