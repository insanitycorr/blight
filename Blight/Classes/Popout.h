#ifndef __POPOUT_H__
#define __POPOUT_H__
#include "cocos2d.h"
#include "InputHandler.h"
#include "Anim.h"

USING_NS_CC;

class Popout {
public:
	Popout();
	Popout(Scene * s, std::string image, Vec2 pos, bool animated);
	~Popout();
	void update(float dt);
	static int getCollisionTag() { return collisionTag; }

private:
	Sprite *attachedSprite;

	std::string path = "ENVIRONMENT/";

	//collisions
	static int collisionTag;

	float slowdowntime;
	Vec2 lerpFrom;
	
	//Animation Controller
	AnimationController* controller;
};

#endif