#ifndef __STATSCREENLAYER_SCENE_H__
#define __STATSCREENLAYER_SCENE_H__

#include "cocos2d.h"
#include "MenuButton.h"

class StatScreenLayer : public cocos2d::Scene
{
public:
	std::vector<Player*> players;
    static cocos2d::Scene* createStatLayer(std::vector<Player*> players);
    virtual bool init();
	void update(float dt);
    // callbacks
    void menuReplayCallback(cocos2d::Ref* pSender, ui::Widget::TouchEventType type);
	void menuLobbyCallback(cocos2d::Ref* pSender, ui::Widget::TouchEventType type);
    // implement the "static create()" method manually
    CREATE_FUNC(StatScreenLayer);
};

#endif // __PAUSEMENU_SCENE_H__
