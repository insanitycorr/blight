#include "TestScene.h"
#include "VictoryLayer.h"
#include "SimpleAudioEngine.h"
#include "SwordHit.h"
#include "EnemyHit.h"
USING_NS_CC;

PhysicsWorld* TestScene::physicsWorld = nullptr;
Scene* TestScene::sceneHandle = nullptr;
std::vector<int> TestScene::controlOrder;
AudioPlayer TestScene::audioP;

Scene* TestScene::createScene()
{
	//Create the actual scene object that gets used with the director. This function is called within AppDelegate.cpp 
	//'scene' is an autorelease object so we never have to call delete on it. If we did, your application would likely crash
	//Important note: Anytime you call ___::create() with Cocos2D, you will be getting an autoreleased object. You do not need to call delete on anything in the Cocos2D engine
	Scene* scene = Scene::createWithPhysics();

	scene->getPhysicsWorld()->setGravity(Vec2::ZERO);

	//Create a layer that is going to be attached to the scene
	//This layer is what contains all of our objects since we are working within a DemoScene
	//Also, when we use 'this->' later on, this layer is what is being referred to
	TestScene* layer = TestScene::create();

	

	//Add the layer to the scene
	scene->addChild(layer);

	//scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);

	//DISPLAY->createDebugConsole(true);
	//Return the newly built scene
	//This is then passed to the director with director->runWithScene() or director->replaceScene() etc. In this case, director->runWithScene() is called in AppDelagate.cpp
	return scene;
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in TestSceneScene.cpp\n");
}

// on "init" you need to initialize your instance
bool TestScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::initWithPhysics() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//health = Label::createWithTTF("Health: 10", "fonts/Marker Felt.ttf", 24);
	//if (health == nullptr)
	//{
	//	problemLoading("'fonts/Marker Felt.ttf'");
	//}
	//else
	//{
	//	// position the label on the center of the screen
	//	health->setPosition(Vec2(origin.x + health->getContentSize().width,
	//		origin.y + visibleSize.height - health->getContentSize().height));
	//
	//	// add the label as a child to this layer
	//	this->addChild(health, 1);
	//}
	//blood = Label::createWithTTF("Blood: 10", "fonts/Marker Felt.ttf", 24);
	//if (blood == nullptr)
	//{
	//	problemLoading("'fonts/Marker Felt.ttf'");
	//}
	//else
	//{
	//	// position the label on the center of the screen
	//	blood->setPosition(Vec2(origin.x + health->getContentSize().width,
	//		origin.y + visibleSize.height - health->getContentSize().height - blood->getContentSize().height));
	//
	//	// add the label as a child to this layer
	//	this->addChild(blood, 1);
	//}

	///Makes it so things can't go flying off the screen
	///Just useful for when testing the contact stuffs
	PhysicsBody* sceneBound = PhysicsBody::createEdgeBox(visibleSize, PHYSICSBODY_MATERIAL_DEFAULT, 10.f);
	Node* sceneNode = Node::create();
	sceneNode->setPosition(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y);
	sceneNode->setPhysicsBody(sceneBound);
	this->addChild(sceneNode);
	///Just comment out this section if you don't want the screen boundries for now, I was just usin em to test contact stuffs
	///And see how everything flowed.
	auto background = Sprite::create("ENVIRONMENT/testRoom.png");
	background->setAnchorPoint(Vec2(0.f, 0.f));
	background->setPosition(origin);
	addChild(background, INT_MIN);

	auto topwall = Node::create();
	auto topwall_body = PhysicsBody::createBox(Size(1920.f,120.f));

	topwall->setPosition(Vec2(0.f, 1080.f - 120.f));
	topwall_body->setDynamic(false);
	topwall_body->setPositionOffset(Vec2(960.f, 60.f));
	topwall->setPhysicsBody(topwall_body);
	addChild(topwall);

	//Once you fix the health from depending on these existing this commented for loop should work
	//I recommend using the vector for the dependancies, the one right down there, players
	/*for (int i = 0; i < Player::numberOfPlayers; i++) {
		players.push_back(new Player(this, Vec2(540 + 200.f*i, 440), i));
	}*/
	
	for (int i = 0; i < controlOrder.size(); i++) { controlCopy.push_back(controlOrder[i]); }

	while (controlCopy.size() < 4) {
		controlCopy.push_back(5);
	}

	Player::controlCon = controlCopy;

	player = new Player(this, Vec2(540, 440), 0);
	player2 = new Player(this, Vec2(740, 440), 1);
	player3 = new Player(this, Vec2(540, 640), 2);
	player4 = new Player(this, Vec2(740, 640), 3);

	players.push_back(player);
	players.push_back(player2);
	players.push_back(player3);
	players.push_back(player4);

	for (int i = Player::numberOfPlayers; i < 4; i++) {
		players[i]->setBoom(false);
		players[i]->setHealth(0, 0);
	}
	player->setHealth(100, 0);


	pillar1 = new Popout(this, "pillar.png", Vec2(175.f, 150.f), false);
	pillar2 = new Popout(this, "pillar.png", Vec2(175.f, 1080.f - (320.f)), false);
	pillar3 = new Popout(this, "pillar.png", Vec2(1920.f-175.f, 150.f), false);
	pillar4 = new Popout(this, "pillar.png", Vec2(1920.f-175.f, 1080.f - (320.f)), false);

	for (int i = 0; i < Player::numberOfPlayers; i++) {
			auto thinger = Sprite::create("UI/HEALTHBAR/" + players[i]->getPath() + "Base.png");
			thinger->setPosition(Vec2(100.f + 200.f*i, 1000.f));
			addChild(thinger, INT_MAX);
			auto thinger2 = Sprite::create("UI/HEALTHBAR/back.png");
			thinger2->setPosition(Vec2(100.f + 200.f*i, 1000.f));
			addChild(thinger2, INT_MAX - 3);
	}

	bar_bossboi = Sprite::create("UI/HEALTHBAR/health.png");
	bar_bossboi->setAnchorPoint(Vec2(0.f, 0.5f));
	bar_bossboi->setPosition(460.f+11.f, 950.f);
	bar_bossboi->setScaleX(1000.f-22.f);
	bar_bossboi->setScaleY(3.f);
	addChild(bar_bossboi, 1);

	bar_bossboi_late = Sprite::create("UI/HEALTHBAR/health_late.png");
	bar_bossboi_late->setAnchorPoint(Vec2(0.f, 0.5f));
	bar_bossboi_late->setPosition(460.f+11.f, 950.f);
	bar_bossboi_late->setScaleX(1000.f-22.f);
	bar_bossboi_late->setScaleY(3.f);
	addChild(bar_bossboi_late, 0);

	bar_bossboi_back = Sprite::create("UI/HEALTHBAR/BOSS/BossBarBack.png");
	bar_bossboi_back->setAnchorPoint(Vec2(.5f, 0.5f));
	bar_bossboi_back->setPosition(960.f, 950.f);
	addChild(bar_bossboi_back, -1);

	setBossBarOpacity(0.8);

	bar_redboi_late = Sprite::create("UI/HEALTHBAR/health_late.png");
	bar_redboi_late->setAnchorPoint(Vec2(0.f, 0.5f));
	bar_redboi_late->setPosition(58.f, 1010.5f);
	bar_redboi_late->setScaleX(114.f*player->getHealth() / 100);
	addChild(bar_redboi_late, INT_MAX - 2);

	bar_greenboi_late = Sprite::create("UI/HEALTHBAR/health_late.png");
	bar_greenboi_late->setAnchorPoint(Vec2(0.f, 0.5f));
	bar_greenboi_late->setPosition(258.f, 1010.5f);
	bar_greenboi_late->setScaleX(114.f*player2->getHealth() / 100);
	addChild(bar_greenboi_late, INT_MAX - 2);

	bar_yellowboi_late = Sprite::create("UI/HEALTHBAR/health_late.png");
	bar_yellowboi_late->setAnchorPoint(Vec2(0.f, 0.5f));
	bar_yellowboi_late->setPosition(458.f, 1010.5f);
	bar_yellowboi_late->setScaleX(114.f*player3->getHealth() / 100);
	addChild(bar_yellowboi_late, INT_MAX - 2);

	bar_blueboi_late = Sprite::create("UI/HEALTHBAR/health_late.png");
	bar_blueboi_late->setAnchorPoint(Vec2(0.f, 0.5f));
	bar_blueboi_late->setPosition(658.f, 1010.5f);
	bar_blueboi_late->setScaleX(114.f*player4->getHealth() / 100);
	addChild(bar_blueboi_late, INT_MAX - 2);

	bar_redboi = Sprite::create("UI/HEALTHBAR/health.png");
	bar_redboi->setAnchorPoint(Vec2(0.f, 0.5f));
	bar_redboi->setPosition(58.f, 1010.5f);
	bar_redboi->setScaleX(114.f*player->getHealth() / 100);
	addChild(bar_redboi, INT_MAX - 1);

	bar_greenboi = Sprite::create("UI/HEALTHBAR/health.png");
	bar_greenboi->setAnchorPoint(Vec2(0.f, 0.5f));
	bar_greenboi->setPosition(258.f, 1010.5f);
	bar_greenboi->setScaleX(114.f*player2->getHealth() / 100);
	addChild(bar_greenboi, INT_MAX - 1);

	bar_yellowboi = Sprite::create("UI/HEALTHBAR/health.png");
	bar_yellowboi->setAnchorPoint(Vec2(0.f, 0.5f));
	bar_yellowboi->setPosition(458.f, 1010.5f);
	bar_yellowboi->setScaleX(114.f*player3->getHealth() / 100);
	addChild(bar_yellowboi, INT_MAX - 1);

	bar_blueboi = Sprite::create("UI/HEALTHBAR/health.png");
	bar_blueboi->setAnchorPoint(Vec2(0.f, 0.5f));
	bar_blueboi->setPosition(658.f, 1010.5f);
	bar_blueboi->setScaleX(114.f*player4->getHealth() / 100);
	addChild(bar_blueboi, INT_MAX - 1);

	bar_redboi_blood = Sprite::create("UI/HEALTHBAR/Blood.png");
	bar_redboi_blood->setAnchorPoint(Vec2(0.f, 0.5f));
	bar_redboi_blood->setPosition(59.f, 998.5f);
	bar_redboi_blood->setScaleX(77.f*1);
	addChild(bar_redboi_blood, INT_MAX - 1);
	
	bar_greenboi_blood = Sprite::create("UI/HEALTHBAR/Blood.png");
	bar_greenboi_blood->setAnchorPoint(Vec2(0.f, 0.5f));
	bar_greenboi_blood->setPosition(259.f, 998.5f);
	bar_greenboi_blood->setScaleX(77.f*player2->getBlood() / 100);
	addChild(bar_greenboi_blood, INT_MAX - 1);
	
	bar_yellowboi_blood = Sprite::create("UI/HEALTHBAR/Blood.png");
	bar_yellowboi_blood->setAnchorPoint(Vec2(0.f, 0.5f));
	bar_yellowboi_blood->setPosition(459.f, 998.5f);
	bar_yellowboi_blood->setScaleX(77.f*player3->getBlood() / 100);
	addChild(bar_yellowboi_blood, INT_MAX - 1);
	
	bar_blueboi_blood = Sprite::create("UI/HEALTHBAR/Blood.png");
	bar_blueboi_blood->setAnchorPoint(Vec2(0.f, 0.5f));
	bar_blueboi_blood->setPosition(659.f, 998.5f);
	bar_blueboi_blood->setScaleX(77.f*player4->getBlood() / 100);
	addChild(bar_blueboi_blood, INT_MAX - 1);

	//Initialise physics.
	initPhysicsCallbacks();
	
	//Test audio stuffs
	//audioP.addAudio("background1", "ENVIRONMENT/");
	audioP.addAudio("background2", "ENVIRONMENT/", 1.f, 1);
	audioP.addAudio("Sword", "SFX/");
	//Testing for next song etc
	//keys.push_back("background2");
	//keys.push_back("background1");

	//Play current element
	audioP.playAudio("background2");
	audioP.setVolume("background2", 0.f);
	

	paused = false;
	this->scheduleUpdate();
    return true;
}

//Initializes the callbacks for physics listeners
void TestScene::initPhysicsCallbacks()
{
	//Create the contact listener
	EventListenerPhysicsContact* contactListener = EventListenerPhysicsContact::create();

	//Assign the callback function
	contactListener->onContactBegin = CC_CALLBACK_1(TestScene::onContactBeginCallback, this);

	//Add the contact listener to the event dispatcher
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);
}

//Callback for when contact begins
bool TestScene::onContactBeginCallback(PhysicsContact& contact)
{
	if (contact.getShapeA()->getBody()->getNode() == nullptr) {
		return true;
	}
	if (contact.getShapeB()->getBody()->getNode() == nullptr) {
		return true;
	}

	Node* nodeA = contact.getShapeA()->getBody()->getNode();
	Node* nodeB = contact.getShapeB()->getBody()->getNode();

	int tagA = nodeA->getPhysicsBody()->getTag();
	int tagB = nodeB->getPhysicsBody()->getTag();

	//Make sure they exist
	if (nodeA && nodeB) {
		//Atm doesn't matter whether player or enemy was tagA or B because only one enemy, but will be nice once testing multiple enemies.
		//Will probably need changing once different enemy types implemented, but will work for testing multiple generic enemies
		if (tagA == SwordHit::getCollisionTag()) {
			if (tagB == Enemy::getCollisionTag()) {
				//We'll need some sort of list of enemies currently around to check the node against it, but again, only one enemy rn
				//nodeB->getPhysicsBody()->setVelocity(Vec2::ZERO);
				float targetx;
				float targety;
				int playerID;
				std::string p = (nodeA)->getName();
				char delimiter = ',';
				std::vector<std::string> tokens;
				std::string token;
				std::istringstream tokenStream(p);
				while (std::getline(tokenStream, token, delimiter))
				{
					tokens.push_back(token);
				}
				targetx = std::stof(tokens[0]);
				targety = std::stof(tokens[1]);
				playerID = std::stoi(tokens[2]);
				


				Player* attacker = nullptr;
				for (int i = 0; i < players.size(); i++) {
					if (playerID == players[i]->getIndex()) {
						attacker = players[i];
					}
				}

				for (int i = 0; i < enemies.size(); i++) {
					if (enemies[i]->getAttachedSprite() == nodeB) {
						enemies[i]->takeDamage(1, Vec2(targetx, targety), 0.f);
						if (enemies[i]->checkIfDead()) {
							if (attacker != nullptr) {
								attacker->setBlood(5, true);
							}
						}
					}
				}
				return false;
			}
		}
		else if (tagB == SwordHit::getCollisionTag()) {
			if (tagA == Enemy::getCollisionTag()) {
				//We'll need some sort of list of enemies currently around to check the node against it, but again, only one enemy rn
				//nodeA->getPhysicsBody()->setVelocity(Vec2::ZERO);
				float targetx;
				float targety;
				int playerID;
				std::string p = (nodeB)->getName();
				char delimiter = ',';
				std::vector<std::string> tokens;
				std::string token;
				std::istringstream tokenStream(p);
				while (std::getline(tokenStream, token, delimiter))
				{
					tokens.push_back(token);
				}
				targetx = std::stof(tokens[0]);
				targety = std::stof(tokens[1]);
				playerID = std::stoi(tokens[2]);

				Player* attacker = nullptr;
				for (int i = 0; i < players.size(); i++) {
					if (playerID == players[i]->getIndex()) {
						attacker = players[i];
					}
				}

				for (int i = 0; i < enemies.size(); i++) {
					if (enemies[i]->getAttachedSprite() == nodeB) {
						enemies[i]->takeDamage(1, Vec2(targetx, targety), 0.f);
						if (enemies[i]->checkIfDead()) {
							if (attacker != nullptr) {
								attacker->setBlood(5, true);
							}
						}
					}
				}
				return false;
			}
		}
	}
	if (nodeA && nodeB) {
		if (tagA == EnemyHit::getCollisionTag()) {
			if (tagB == Player::getCollisionTag()) {
				if (nodeB->getName() == "0" && !player->isBoss())
					player->setHealth(-std::stoi(nodeA->getName()), 1);
				else if (nodeB->getName() == "1" && !player2->isBoss())
					player2->setHealth(-std::stoi(nodeA->getName()), 1);
				else if (nodeB->getName() == "2" && !player3->isBoss())
					player3->setHealth(-std::stoi(nodeA->getName()), 1);
				else if (nodeB->getName() == "3" && !player4->isBoss())
					player4->setHealth(-std::stoi(nodeA->getName()), 1);
				return false;
			}
		}
		else if (tagB == EnemyHit::getCollisionTag()) {
			if (tagA == Player::getCollisionTag()) {
				if (nodeA->getName() == "0" && !player->isBoss())
					player->setHealth(-std::stoi(nodeB->getName()), 1);
				else if (nodeA->getName() == "1" && !player2->isBoss())
					player2->setHealth(-std::stoi(nodeB->getName()), 1);
				else if (nodeA->getName() == "2" && !player3->isBoss())
					player3->setHealth(-std::stoi(nodeB->getName()), 1);
				else if (nodeA->getName() == "3" && !player4->isBoss())
					player4->setHealth(-std::stoi(nodeB->getName()), 1);
				return false;
			}
		}
	}
	if (nodeA && nodeB) {
		//Atm doesn't matter whether player or enemy was tagA or B because only one enemy, but will be nice once testing multiple enemies.
		//Will probably need changing once different enemy types implemented, but will work for testing multiple generic enemies
		if (tagA == SwordHit::getCollisionTag()) {
			if (tagB == Player::getCollisionTag()) {
				//We'll need some sort of list of enemies currently around to check the node against it, but again, only one enemy rn
				for (int i = 0; i < players.size(); i++) {
					if (players[i]->getAttachedSprite() == nodeB) {
						if (players[i]->isBoss()) {
							players[i]->setBossHealth(-0.05f, 1);
						}
					}
				}
				return false;
			}
		}
		else if (tagB == SwordHit::getCollisionTag()) {
			if (tagA == Player::getCollisionTag()) {
				//We'll need some sort of list of enemies currently around to check the node against it, but again, only one enemy rn
				for (int i = 0; i < players.size(); i++) {
					if (players[i]->getAttachedSprite() == nodeA) {
						if (players[i]->isBoss()) {
							players[i]->setBossHealth(-0.05f,1);
						}
					}
				}
				return false;
			}
		}
	}
	if (nodeA && nodeB) {
		//Atm doesn't matter whether player or enemy was tagA or B because only one enemy, but will be nice once testing multiple enemies.
		//Will probably need changing once different enemy types implemented, but will work for testing multiple generic enemies
		if (tagA == EnemyHit::getCollisionTag()) {
			if (tagB == Enemy::getCollisionTag()) {
				//We'll need some sort of list of enemies currently around to check the node against it, but again, only one enemy rn
				return false;
			}
		}
		else if (tagB == EnemyHit::getCollisionTag()) {
			if (tagA == Enemy::getCollisionTag()) {
				//We'll need some sort of list of enemies currently around to check the node against it, but again, only one enemy rn
				return false;
			}
		}
	}
//Make sure they exist
if (nodeA && nodeB) {
	//Atm doesn't matter whether player or enemy was tagA or B because only one enemy, but will be nice once testing multiple enemies.
	//Will probably need changing once different enemy types implemented, but will work for testing multiple generic enemies
	if (tagA == Player::getCollisionTag()) {
		if (tagB == Enemy::getCollisionTag()) {
			//We'll need some sort of list of enemies currently around to check the node against it, but again, only one enemy rn
			return true;
		}
	}
	else if (tagB == Player::getCollisionTag()) {
		if (tagA == Enemy::getCollisionTag()) {
			//We'll need some sort of list of enemies currently around to check the node against it, but again, only one enemy rn
			return true;
		}
	}
}
return true;
}

void TestScene::update(float dt) {
	printf("%i", players.size());
	if ((int)players.size() == 1) {
		if (!paused) {
			paused = true;
			addChild(VictoryLayer::createPauseLayer(&paused, 0, players[0]->getIndex()));
		}
	}
	if (fadeOut) {
		fadeOut = audioP.fadeOut("background1", 4);
	}
	if (fadeIn) {
		fadeIn = audioP.fadeIn("background2", 4);
	}
	XINPUT->DownloadPackets(4);
	for (int i = 0; i < 4; i++)
		XINPUT->SetVibration(i, 0.f, 0.f);

	{
		bool bossFound = false;
		for (int i = 0; i < players.size(); i++) {
			if (players[i]->isBoss()) {
				setBossBarOpacity(0.8f);
				bossFound = true;
				bar_bossboi->setScaleX((1000 - 22)*players[i]->getBossHealth());
			}
		}
		if (!bossFound) {
			setBossBarOpacity(0.f);
			spawnWaves = true;
		}
	}
	//Update the health bar
	bar_redboi->setScaleX(114.f*player->getHealth() / 100);
	if (bar_redboi_late->getScaleX() > bar_redboi->getScaleX()) {
		bar_redboi_late->setScaleX(bar_redboi_late->getScaleX() - 50 * dt);
		XINPUT->SetVibration(controlOrder[0], 1.f, 1.f);
	}
	else bar_redboi_late->setScaleX(bar_redboi->getScaleX());

	bar_greenboi->setScaleX(114.f*player2->getHealth() / 100);
	if (bar_greenboi_late->getScaleX() > bar_greenboi->getScaleX()) {
		bar_greenboi_late->setScaleX(bar_greenboi_late->getScaleX() - 50 * dt);
		XINPUT->SetVibration(controlOrder[1], 1.f, 1.f);
	}
	else bar_greenboi_late->setScaleX(bar_greenboi->getScaleX());

	bar_yellowboi->setScaleX(114.f*player3->getHealth() / 100);
	if (bar_yellowboi_late->getScaleX() > bar_yellowboi->getScaleX()) {
		bar_yellowboi_late->setScaleX(bar_yellowboi_late->getScaleX() - 50 * dt);
		XINPUT->SetVibration(controlOrder[2], 1.f, 1.f);
	}
	else bar_yellowboi_late->setScaleX(bar_yellowboi->getScaleX());

	bar_blueboi->setScaleX(114.f*player4->getHealth() / 100);
	if (bar_blueboi_late->getScaleX() > bar_blueboi->getScaleX()) {
		bar_blueboi_late->setScaleX(bar_blueboi_late->getScaleX() - 50 * dt);
		XINPUT->SetVibration(controlOrder[3], 1.f, 1.f);
	}
	else bar_blueboi_late->setScaleX(bar_blueboi->getScaleX());

	//Update the blood bars

	bar_redboi_blood->setScaleX(77.f*player->getBlood() / 100);
	bar_greenboi_blood->setScaleX(77.f*player2->getBlood() / 100);
	bar_yellowboi_blood->setScaleX(77.f*player3->getBlood() / 100);
	bar_blueboi_blood->setScaleX(77.f*player4->getBlood() / 100);



	for (int i = 0; i < players.size(); i++) {
		if (players[i]->getHealth() <= 0)
			players.erase(players.begin()+i);
	}
	pauseDelay -= dt;
	for (int i = 0; i < controlOrder.size(); i++) {
		if (XINPUT->GetButton(controlOrder[i], Button::Start) && pauseDelay <= 0) {
			pauseDelay = 0.5f;
			pause(controlOrder[i]);
		}
	}
	/*if (INPUTS->getKeyPress(KeyCode::KEY_ESCAPE)) {
		pause();
	}*/

	if (!paused) {
		if (spawnWaves) {
			waveTimer -= dt;
			if (waveTimer <= 0) {
				waveTimer = 10.f;
				if (wave == 1) {
					for (int i = 0; i < 2; i++) {
						for (int j = 0; j < 2; j++) {
							enemies.push_back(new Ghoul(this, Vec2(960 - 300 + 400 * i, 540 - 200 + 100 * j), nullptr));
						}
					}
				}
				else if (wave == 2) {
					for (int i = 0; i < 3; i++) {
						for (int j = 0; j < 2; j++) {
							enemies.push_back(new Ghoul(this, Vec2(960 - 200 + 400 * i, 540 - 100 + 200 * j), nullptr));
						}
					}
					enemies.push_back(new Ravager(this, Vec2(960.f, 540.f), nullptr));
				}
				else if (wave == 3) {
					for (int i = 0; i < 2; i++) {
						for (int j = 0; j < 2; j++) {
							enemies.push_back(new Ghoul(this, Vec2(960 - 200 + 400 * i, 540 - 100 + 200 * j), nullptr));
						}
					}
					enemies.push_back(new Ravager(this, Vec2(960 - 40.f, 540.f), nullptr));
					enemies.push_back(new Ravager(this, Vec2(960 + 40.f, 540.f), nullptr));
				}
				else if (wave == 4) {
					for (int i = 0; i < 3; i++) {
						for (int j = 0; j < 3; j++) {
							enemies.push_back(new Ghoul(this, Vec2(960 - 200 + 200 * i, 540 - 100 + 100 * j), nullptr));
						}
					}
					enemies.push_back(new Ravager(this, Vec2(960 - 40.f, 540.f - 50), nullptr));
					enemies.push_back(new Ravager(this, Vec2(960 + 40.f, 540.f - 50), nullptr));
					enemies.push_back(new Ravager(this, Vec2(960.f, 540.f + 50), nullptr));
					wave--;
				}
				wave++;
			}
		}

		//If the boss exists, ensure that waves don't spawn, and that all enemies on the screen die.
		if (spawnWaves) {
			for (int i = 0; i < players.size(); i++) {
				if (players[i]->isBoss()) {
					spawnWaves = false;
					wave = 1;
					waveTimer = 10.f;
					for (int j = 0; j < enemies.size(); j++) {
						enemies[j]->takeDamage(100);
					}
				}
			}
		}

		//Damage the boss over time.
		for (int i = 0; i < players.size(); i++) {
			if (players[i]->isBoss()) {
				players[i]->setBossHealth(-0.0333*dt, 1);
			}
		}

		//Checks if enemy dead, then "kills" (read kill comments)
		for (int i = 0; i < enemies.size(); i++) {
			if (enemies[i]->checkIfDead()) {
				enemies[i]->kill();
				enemies.erase(enemies.begin() + i);
			}
		}

		for (int i = 0; i < getChildren().size(); i++) {
			Node *n = getChildren().at(i);
			if (n->getTag() == SwordHit::getCollisionTag()) {
				n->removeFromParentAndCleanup(1);
			}
			else if (n->getTag() == EnemyHit::getCollisionTag()) {
				n->removeFromParentAndCleanup(1);
			}
		}
		for (int i = 0; i < players.size(); i++) {
			players[i]->update(dt,players,playersDead);
		}
		for (int i = 0; i < enemies.size(); i++) {
				enemies[i]->update(dt, players);
		}
		pillar1->update(dt);
		pillar2->update(dt);
		pillar3->update(dt);
		pillar4->update(dt);
	}
	
	if (INPUTS->getKeyPress(KeyCode::KEY_0)) { debugToggle(); }
	if (debugOn) { audioDebug(); }

	INPUTS->clearForNextFrame();
}

//Pauses such that all actions are paused and then resumed when the the pause is done
//Music is also paused because ew no
void TestScene::pause(int _controller)
{
	paused = !paused;
	//paused ? addChild(PauseMenuLayer::getInstance()) : removeChild(PauseMenuLayer::getInstance());
	if (paused) {
		Director::getInstance()->getRunningScene()->getPhysicsWorld()->setSpeed(0.f);
		pausedAc = Director::getInstance()->getActionManager()->pauseAllRunningActions();

		audioP.setVolume("background2", 0.45f);
		auto pauseLayer = PauseMenuLayer::createPauseLayer(&paused, _controller);
		addChild(pauseLayer, INT_MAX);
	}
	else {
		audioP.setVolume("background2", 1.0f);
		Director::getInstance()->getRunningScene()->getPhysicsWorld()->setSpeed(1.f);
		Director::getInstance()->getActionManager()->resumeTargets(pausedAc);
	}
}

void TestScene::debugToggle()
{
	debugOn = !(debugOn);
}

void TestScene::audioDebug()
{
	//plays background music
	if (INPUTS->getKeyPress(KeyCode::KEY_1)) {
		audioP.playAudio(keys[element]);
	}
	//stops background music
	if (INPUTS->getKeyPress(KeyCode::KEY_2)) {
		audioP.stopAudio(keys[element]);
	}
	//pauses background music
	if (INPUTS->getKeyPress(KeyCode::KEY_3)) {
		audioP.pauseAudio(keys[element]);
	}
	//resumes background music
	if (INPUTS->getKeyPress(KeyCode::KEY_4)) {
		audioP.resumeAudio(keys[element]);
	}
	//next background song
	if (INPUTS->getKeyPress(KeyCode::KEY_5)) {
		audioP.stopAudio(keys[element]);
		keys.size() - 1 > element ? element += 1 : element -= 1;
		audioP.playAudio(keys[element]);
	}
	if (INPUTS->getKeyPress(KeyCode::KEY_EQUAL)) {
		audioP.volumeUp(keys[element]);
	}
	if (INPUTS->getKeyPress(KeyCode::KEY_MINUS)) {
		audioP.volumeDown(keys[element]);
	}
	//if (INPUTS->getKeyPress(KeyCode::KEY_O)) {
	//	player->setHealth(-10, 1);
	//}
	//if (INPUTS->getKeyPress(KeyCode::KEY_P)) {
	//	player->setHealth(-5, 1);
	//}
	//if (INPUTS->getKeyPress(KeyCode::KEY_9)) {
	//	player->setHealth(100, 0);
	//}
}

//void TestScene::menuCloseCallback(Ref* pSender)
//{
//    //Close the cocos2d-x game scene and quit the application
//    Director::getInstance()->end();
//
//    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
//    exit(0);
//#endif
//
//    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/
//
//    //EventCustom customEndEvent("game_scene_close_event");
//    //_eventDispatcher->dispatchEvent(&customEndEvent);
//
//	INPUTS->clearForNextFrame();
//}

