#ifndef __MENUBUTTON_H__
#define __MENUBUTTON_H__

#include "cocos2d.h"
#include "cocos\ui\CocosGUI.h"
USING_NS_CC;

class MenuButton {
public:
	MenuButton();
	MenuButton(Scene* scene, std::string name = "", Vec2 pos = Vec2::ZERO);
	ui::Button *getButton() {
		return btn;
	}
private:
	ui::Button *btn;
};

#endif