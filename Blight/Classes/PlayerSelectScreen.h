#ifndef __PLAYERSELECTSCREEN_SCENE_H__
#define __PLAYERSELECTSCREEN_SCENE_H__

#include "cocos2d.h"
#include "MenuButton.h"
#include "TestScene.h"
#include "InputHandler.h"
#include <vector>



class PlayerSelectScreen : public cocos2d::Scene
{
	//Ghetto fix for "get button press"
	bool lastLeftState[4] = { false, false, false, false };
	bool lastRightState[4] = { false, false, false, false };
	bool lastConfirmState[4] = { false, false, false, false };
	//Which controllers joined in
	bool controllersIn[4] = { false, false, false, false };

	//Which players joined in
	bool playersIn[4] = { false, false, false, false };
	//Players that are ready, will be checked against players that are there so if players[0] is true
	//Then it checks if readyPlayer[0] is also true and so on for every true in array players
	bool readyPlayer[4] = { false, false, false, false };
	Vec2 playerPos[4] = { Vec2(200, 350), Vec2(670, 350), Vec2(1190, 350), Vec2(1700, 350) };
	std::vector<std::string> paths; //= {"HERO/", "HERO_GREEN/", "HERO_YELLOW/", "HERO_BLUE/"};
	bool enablePlay;
	//does basically the same as enable play but ONLY enables back button
	bool enableBack;
	float delay;

	bool fadeIn = false;
	bool fadeOut = false;
	
	Label* prompt;
	Label* prompt2;

	int bothTrue = 0;
	int oneTrue = 0;

	Sprite* highlightButton;
	Sprite* play;
	Sprite* back;
	
	Sprite* sprites[4];
	Size visibleSize;
	int maxAmount = 3;
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	void update(float dt);
	// a selector callback
	void menuBackCallback(cocos2d::Ref* pSender);
	void menuPlayCallback(cocos2d::Ref* pSender/*, ui::Widget::TouchEventType type*/);
	// implement the "static create()" method manually
	
	void checkSkindex(int, std::string);

	CREATE_FUNC(PlayerSelectScreen);
};

#endif // __PLAYERSELECTSCREEN_SCENE_H__
#pragma once
