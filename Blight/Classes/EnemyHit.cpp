#include "EnemyHit.h"
#include "Shape.h"

int EnemyHit::collisionTag = 5;

EnemyHit::EnemyHit() {
	node = nullptr;
}

EnemyHit::EnemyHit(Scene* s, Vec2 pos, Size size, int damage) {
	node = Node::create();
	node->setPosition(pos);
	node->setTag(EnemyHit::collisionTag);
	auto phys = PhysicsBody::createBox(size);
	phys->setPositionOffset(Vec2(size.width*0.5f, size.height*.5f));
	phys->setDynamic(false);
	phys->setTag(EnemyHit::collisionTag);
	phys->setContactTestBitmask(0xFFFFFFFF);
	node->setPhysicsBody(phys);
	node->getPhysicsBody()->setName(std::to_string(damage));
	node->setName(std::to_string(damage));

	s->addChild(node);
}

EnemyHit::~EnemyHit() {
	node = nullptr;
}