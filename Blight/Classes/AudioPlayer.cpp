#include "AudioPlayer.h"
#define PLAYING AudioEngine::AudioState::PLAYING
#define INITIALIZING AudioEngine::AudioState::INITIALIZING
#define ERROR AudioEngine::AudioState::ERROR
#define PAUSED AudioEngine::AudioState::PAUSED

AudioPlayer::AudioPlayer()
{
}

AudioPlayer::~AudioPlayer()
{
}

void AudioPlayer::addAudio(std::string _key, std::string _path, float _volume, bool _loop, AudioProfile * _profile)
{
	if (holder.find(_key) == holder.end()) {
		MusicHold temp(_key, _path, _loop, _volume, _profile);
		holder.insert(std::make_pair(_key, temp));
	}
}

void AudioPlayer::playAudio(std::string key)
{
	MusicHold temp = holder[key];
	if (audioIDs.find(key) == audioIDs.end()) {
		audioIDs.insert(std::make_pair(key, AudioEngine::play2d(temp.path, temp.loop, temp.volume, temp.profile)));
	}
	else {
		audioIDs[key] = AudioEngine::play2d(temp.path, temp.loop, temp.volume, temp.profile);
	}
}

void AudioPlayer::stopAudio(std::string key)
{
	if (audioIDs.find(key) == audioIDs.end()) {
		return;
	}
	AudioEngine::stop(audioIDs[key]);
	audioIDs.erase(key);
}

void AudioPlayer::stopAllAudio()
{
	audioIDs.clear();
	AudioEngine::stopAll();
}

void AudioPlayer::pauseAudio(std::string key)
{
	AudioEngine::pause(audioIDs[key]);
}

void AudioPlayer::resumeAudio(std::string key)
{
	AudioEngine::resume(audioIDs[key]);
}

void AudioPlayer::setVolume(std::string key, float _volume)
{
	AudioEngine::setVolume(audioIDs[key], _volume);
	holder[key].volume = _volume;
}

float AudioPlayer::getVolume(std::string key)
{
	return AudioEngine::getVolume(audioIDs[key]);
}

bool AudioPlayer::checkIfPlaying(std::string key)
{
	if (holder.find(key) == holder.end()) {
		return false;
	}
	else if (audioIDs.find(key) == audioIDs.end()) {
		return false;
	}
	else {
		AudioEngine::AudioState state = AudioEngine::getState(audioIDs[key]);
		if (state == PLAYING) {
			return true;
		}
	}
	return false;
}

void AudioPlayer::volumeDown(std::string key)
{
	float temp = holder[key].volume;
	if (temp != 0.f) {
		temp -= 0.05f;
		AudioEngine::setVolume(audioIDs[key], temp);
		holder[key].volume = temp;
	}
}

void AudioPlayer::volumeUp(std::string key)
{
	float temp = holder[key].volume;
	if (temp != 1.f) {
		temp += 0.05f;
		AudioEngine::setVolume(audioIDs[key], temp);
		holder[key].volume = temp;
	}
}

//The bool signals it's not fading anymore
bool AudioPlayer::fadeOut(std::string key, int delay)
{
	if (counterOut.find(key) == counterOut.end()) {
		counterOut.insert(std::make_pair(key, 0));
	}
	if (counterOut[key] >= delay) {
		counterOut[key] = 0;
		if (getVolume(key) > 0.f) {
			volumeDown(key);
			return true;
		}
		else if (getVolume(key) == 0.f) {
			stopAudio(key);
			counterOut.erase(key);
			return false;
		}
	}
	else {
		counterOut[key]++;
	}
	
}

//The bool signals it's not fading anymore
bool AudioPlayer::fadeIn(std::string key, int delay)
{
	if (counterIn.find(key) == counterIn.end()) {
		counterIn.insert(std::make_pair(key, 0));
	}
	if (counterIn[key] >= delay) {
		counterIn[key] = 0;
		if (getVolume(key) < 1.f) {
			volumeUp(key);
			return true;
		}
		else if (getVolume(key) == 1.f) {
			counterIn.erase(key);
			return false;
		}
	}
	else {
		counterIn[key]++;
	}
}
