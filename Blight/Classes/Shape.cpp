
#include "Shape.h"

// Constructor
C_Shape::C_Shape(Scene* _parentScene, Vec2 _position, float _rotation, Color4F _color)
{
	// Create DrawNode
	drawNode = DrawNode::create();
	// Add to scene
	parentScene = _parentScene;
	parentScene->addChild(drawNode, INT_MAX);

	// Set Data
	setPosition(_position);
	setRotation(_rotation);
	setColor(_color);
	// Anchor point in center
	drawNode->setAnchorPoint(Vec2(0.5f, 0.5f));
}

C_Shape::C_Shape(Node* _parentNode, Vec2 _position, float _rotation, Color4F _color)
{
	// Create DrawNode
	drawNode = DrawNode::create();
	// Add to scene
	parentScene = _parentNode->getScene();
	_parentNode->addChild(drawNode);

	// Set Data
	setPosition(_position);
	setRotation(_rotation);
	setColor(_color);
	// Anchor point in center
	drawNode->setAnchorPoint(Vec2(0.5f, 0.5f));
}
C_Shape::~C_Shape()
{
	clear();
//	parentScene->removeChild(drawNode);
	drawNode->removeFromParentAndCleanup(1);
	drawNode = nullptr;
}