#ifndef __ANIM_H__
#define __ANIM_H__
#include "cocos2d.h"
#include <cfguard.h>

USING_NS_CC;

class AnimationController
{
public:

	AnimationController();
	AnimationController(Sprite*);

	~AnimationController();
	
	//**First Parameter is the key given when you created the specific animation
	//**Second Parameter is for a special action, say you want the object to have 
	//some other action called directly after the animation completes, this
	//allows you to do that.
	void runAnimation(std::string, Action* = nullptr);
	void runAnimationIfReady(std::string, Action* =nullptr);

	//**First Parameter is the Key used to access the animation when you run it
	//**Second Parameter is a string that holds the path for the class calling the AnimationController
	//**Third Parameter is the number of loops you want the animation to do, default -1 is infinite
	//**Fourth Parameter is the duration that each individual frame is on screen, default is 0.1367
	//***********Make sure that all animation frames are in PLAYER/ANIMATIONS*************
	void addAnimation(std::string, std::string, int=-1, float=0.1367f);

private:
	Animation* anim;
	Sprite* refSprite;
	std::unordered_map<std::string, Animation*> animations;
};

#endif