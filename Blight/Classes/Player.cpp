#include "Player.h"
#include "SwordHit.h"
#include "EnemyHit.h"

float Player::speedcap = 512.f;
float Player::accelleration = 1000.f;
int Player::collisionTag = 4;
//Will be changed in level select screen scene
bool Player::someoneBoss = false;
int Player::numberOfPlayers = 0;
int Player::skindex[4] = { 0, 1, 2, 3 };
std::vector<int> Player::controlCon;

Player::Player() {
	attachedSprite = nullptr;
}

Player::Player(Scene* scene, Vec2 pos, int playerindex) {
	//Create sprite.
	attachedSprite = Sprite::create(paths[skindex[0]]+"ANIMATIONS/IdleRight/IdleRight1.png");
	attachedSprite->setPosition(pos);
	attachedSprite->setAnchorPoint(Vec2(.5f, .5f));
	//I want the explosions bigger so scaled sprites just for a bit
	attachedSprite->setScale(1.0f);
	
	bossHealth = 1.f;
	////Add a shadow
	//auto shadow = Sprite::create("HERO/Shadow.png");
	//shadow->setAnchorPoint(Vec2(.5f, 0.f));
	//shadow->setPosition(256.f, 0.f);
	//attachedSprite->addChild(shadow,-1);

	scene->addChild(attachedSprite, 0);

	//Create a basic physics body for sprite and attach it.
	Size box = attachedSprite->getContentSize();
	box.width /= 6;
	box.height /= 20;

	health = 100;
	blood = 0;
	healcd = 0;
	boss = false;

	PhysicsBody* physicsBody = PhysicsBody::createBox(box);
	physicsBody->setPositionOffset(Vec2(0.0f, box.height*.5f));
	physicsBody->setDynamic(true);
	physicsBody->setTag(collisionTag);
	physicsBody->setContactTestBitmask(0xFFFFFFFF);
	physicsBody->setRotationEnable(false);
	physicsBody->setLinearDamping(0.2f);
	attachedSprite->setPhysicsBody(physicsBody);

	//Create your animations here
	controller = new AnimationController(attachedSprite);
	this->playerindex = playerindex;
	if (playerindex == 0)
		path = paths[skindex[0]];
	if (playerindex == 1)
		path = paths[skindex[1]];
	if (playerindex == 2)
		path = paths[skindex[2]];
	if (playerindex == 3)
		path = paths[skindex[3]];
	



	if (playerindex == 0) attachedSprite->setName("0");
	if (playerindex == 1) attachedSprite->setName("1");
	if (playerindex == 2) attachedSprite->setName("2");
	if (playerindex == 3) attachedSprite->setName("3");

	controller->addAnimation("IdleRight", Player::path, -1, 0.2f);
	controller->addAnimation("WalkRight", Player::path, -1, 0.2f);
	controller->addAnimation("WalkUp", Player::path, -1, 0.2f);
	controller->addAnimation("AttackRightA", Player::path, -1, 0.2f);
	controller->addAnimation("AttackUpA", Player::path, -1, 0.2f);
	controller->addAnimation("AttackDownA", Player::path, -1, 0.2f);
	controller->addAnimation("AttackRightB", Player::path, -1, 0.2f);
	controller->addAnimation("HealRight", Player::path, -1, 0.1f);
	controller->addAnimation("HopRight", Player::path, -1, 0.2f);

	//Boss animations
	controller->addAnimation("BossIdleRight", Player::path, -1, 0.5f);
	controller->addAnimation("BossWalkRight", Player::path, -1, 0.2f);
	controller->addAnimation("BossWalkUp", Player::path, -1, 0.2f);
	controller->addAnimation("BossAttack", Player::path, -1, 0.4f);
	controller->addAnimation("BossSpawn", Player::path, 1, 0.5f);
	controller->addAnimation("BossDeath", Player::path, 1, 0.5f);

	controller->runAnimation("IdleRight");


	audioP.addAudio("Sword", "SFX/");

	lerpFrom = Vec2::ZERO;
	slowdowntime = 0.f;
	
	sceneHandle = scene;
}

Player::~Player() {
	attachedSprite = nullptr;
}

void Player::update(float dt, std::vector<Player*> players, std::vector<Player*>& deadPlayers) {
	if (stunTimer > 0.f) {
		stunTimer -= dt;
		if (stunTimer <= 0.f)
			controller->runAnimation(isBoss() ? "BossIdleRight" : "IdleRight");
		//Slow down and stop.
		slowdowntime += dt * 4.0f;
		if (slowdowntime > 1.0f) slowdowntime = 1.0f;
		attachedSprite->getPhysicsBody()->setVelocity(lerpFrom.lerp(Vec2::ZERO, slowdowntime));
		return;
	}
	std::string spritePrefix = isBoss() ? "Boss" : "";
	if (health <= 0) return;
	timeAlive += dt;
	if (healcd > 0.f) {
		healcd -= dt;
	}
	XINPUT->DownloadPackets(5);
	//The current direction the player is accelerating in.
	if (attackStampDelay > 0.f) {
		attackStampDelay -= dt;
		if (attackStampDelay <= 0.f) {
			if (isBoss()) {
				//{auto s = new SwordHit(sceneHandle, Vec2(attachedSprite->getPosition().x - 400.f, attachedSprite->getPosition().y - 150), Size(800, 400), attachedSprite); }
				{auto s = new EnemyHit(sceneHandle, Vec2(attachedSprite->getPosition().x - 400.f, attachedSprite->getPosition().y - 150), Size(800, 400), 5);}
				//{auto s = new SwordHit(sceneHandle, Vec2(attachedSprite->getPosition().x - 220.f, attachedSprite->getPosition().y - 50), Size(440, 200), attachedSprite); }
				{auto s = new EnemyHit(sceneHandle, Vec2(attachedSprite->getPosition().x - 220.f, attachedSprite->getPosition().y - 50), Size(440, 200), 5);}
			} else if (dir.y > 0.2f) {
				//up
				auto s = new SwordHit(sceneHandle, Vec2(attachedSprite->getPosition().x-30, attachedSprite->getPosition().y), Size(70, 150), attachedSprite);
			}
			else if (dir.y < -0.2f) {
				//down
				auto s = new SwordHit(sceneHandle, Vec2(attachedSprite->getPosition().x-30, attachedSprite->getPosition().y-50), Size(70, 90), attachedSprite);
			}
			else {
				if (attachedSprite->isFlippedX()) {
					//left
					auto s = new SwordHit(sceneHandle, Vec2(attachedSprite->getPosition().x - 180, attachedSprite->getPosition().y-30), Size(150, 100), attachedSprite);
				}
				else {
					//right
					auto s = new SwordHit(sceneHandle, Vec2(attachedSprite->getPosition().x + 30, attachedSprite->getPosition().y-30), Size(150, 100), attachedSprite);
				}
			}
		}
	}
	if (!attackAnim) {
		dir = Vec2::ZERO;
		for (int i = 0; i < 4; i++) {
			if (playerindex == i) {
				if (XINPUT->GetConnected(controlCon[i])) {
					Stick lStick, rStick;
					XINPUT->GetSticks(controlCon[i], lStick, rStick);
					if (XINPUT->GetButton(controlCon[i], Button::DPadUp)) {
						dir.y += 1;
					}
					if (XINPUT->GetButton(controlCon[i], Button::DPadLeft)) {
						dir.x -= 1;
					}
					if (XINPUT->GetButton(controlCon[i], Button::DPadDown)) {
						dir.y -= 1;
					}
					if (XINPUT->GetButton(controlCon[i], Button::DPadRight)) {
						dir.x += 1;
					}
					if (lStick.xAxis > 0.3f || lStick.yAxis > 0.3f || lStick.xAxis < -0.3f || lStick.yAxis < -0.3f) {
						dir.x = lStick.xAxis;
						dir.y = lStick.yAxis;
					}
					float lt=0.0f, rt=0.0f;
					if (!someoneBoss) {
						XINPUT->GetTriggers(controlCon[i], lt, rt);
						if (rt > 0.5f) {
							if (blood == 100.f) {
								bool canBeBoss = true;
								//for (int i = 0; players.size(); i++) {
								//	if (players[i]->isBoss()) {
								//		canBeBoss = false;
								//	}
								//}
								if (canBeBoss) {
									setBoss(true);
									blood = 0.f;
								}
							}
						}
					}
					if (XINPUT->GetButton(controlCon[i], Button::Y)) {
						if (healcd <= 0.f && !isBoss()) {
							if (getBlood() >= 50) {
								setBlood(-50, 1);
								setHealth(10, 1);
								healcd = 0.5f;
								stunTimer = 1.0f;
								controller->runAnimation("HealRight");
								moving = false;
								auto healParticle = ParticleFlower::create();
								healParticle->setDuration(0.5);
								healParticle->setSpeed(150.f);
								healParticle->setSpeedVar(0.f);
								healParticle->setStartColor(Color4F::GREEN);
								healParticle->setEndColor(Color4F(0, 0, 0, 0));
								Vec2 pos = Vec2::ZERO;
								pos.x += 320.f;
								pos.y += 696.f;
								attachedSprite->addChild(healParticle);
								healParticle->setPosition(pos);
							}
						}
					}

					if (stunTimer > 0.f) return;

					if (XINPUT->GetButton(controlCon[i], Button::X)) {
						if (!attacking)
						{
							if (attackedLastFrame) {
								altAttack = !altAttack;
							}
							attacking = true;
							attackTimer = 0.6f;
							attackStampDelay = 0.2f;

							if (isBoss())
							{
								attackTimer = 1.2f;
								attackStampDelay = 0.4f;
							}

							audioP.playAudio("Sword");
						}
					}

				}
			}
		}
	}
	//Normalise the direction vector.
	if (dir.length()>1.f) dir.normalize();
	if (attacking) {
		attackedLastFrame = true;
		attackTimer -= dt;
		if (attackTimer<=0.f) {
			attacking = false;
			attackAnim = false;
			controller->runAnimation(spritePrefix + "IdleRight");
		}
		else {
			//Slow down and stop.
			slowdowntime += dt * 4.0f;
			if (slowdowntime > 1.0f) slowdowntime = 1.0f;
			attachedSprite->getPhysicsBody()->setVelocity(lerpFrom.lerp(Vec2::ZERO, slowdowntime));
			//Animate Attack
			if (!attackAnim) {
				attackAnim = true;
				moving = false;
				if (isBoss()) {
					controller->runAnimation("BossAttack");
				} else if (dir.y>0.4f) {
					controller->runAnimation("AttackUpA");
				} else if (dir.y<-0.4f) {
					controller->runAnimation("AttackDownA");
				} else {
					controller->runAnimation("AttackRightA");
					if (dir.x < -0.4f)
						attachedSprite->setFlippedX(true);
					if (dir.x > 0.4f)
						attachedSprite->setFlippedX(false);
				}
			}
		}
	} else {
		attackedLastFrame = false;
		if (attackAnim) {
			moving = false;
			attackAnim = false;
			controller->runAnimation(spritePrefix + "IdleRight");
		}

		//Calculate and apply acceleration.
		Vec2 acc(0, 0);
		acc.x = dir.x*Player::speedcap*dt*4.0f;
		acc.y = dir.y*Player::speedcap*dt*4.0f;
		attachedSprite->getPhysicsBody()->setVelocity(attachedSprite->getPhysicsBody()->getVelocity() + acc);

		//Cap the speed.
		if (attachedSprite->getPhysicsBody()->getVelocity().length() > Player::speedcap) {
			Vec2 temp = attachedSprite->getPhysicsBody()->getVelocity();
			temp.normalize();
			temp *= Player::speedcap;
			attachedSprite->getPhysicsBody()->setVelocity(temp);
		}

		if (acc.length() > 0) {
			//Do not slow down and reset slowdowntime variable/lerp start.
			slowdowntime = 0.0f;
			lerpFrom = attachedSprite->getPhysicsBody()->getVelocity();
			//Animate Walking
			if (!moving) {
				moving = true;
				controller->runAnimation(spritePrefix + "WalkRight");
			}
			if (moving) {
				if (!up && acc.y > 0) {
					up = true;
					if (isBoss()) {
						controller->runAnimation("BossWalkRight");
					} else {
						controller->runAnimation("WalkUp");
					}
				}
				if (up && acc.y < 0) {
					up = false;
					controller->runAnimation(spritePrefix + "WalkRight");
				}
			}
			
			if (dir.x < 0) {
				attachedSprite->setFlippedX(true);
			}
			else if (dir.x > 0) {
				attachedSprite->setFlippedX(false);
			}
			
		}
		else {
			//Slow down and stop.
			slowdowntime += dt * 4.0f;
			if (slowdowntime > 1.0f) slowdowntime = 1.0f;
			attachedSprite->getPhysicsBody()->setVelocity(lerpFrom.lerp(Vec2::ZERO, slowdowntime));
			//Animate Idle
			if (moving) {
				moving = false;
				controller->runAnimation(spritePrefix + "IdleRight");
			}
		}
	}
	//The depth effect.
	attachedSprite->setZOrder(-(attachedSprite->getPosition().y));
	
}

void Player::setBoss(bool b) {
	boss = b;
	if (boss) {
		//someoneBoss = true;
		//Gaining boss status
		Size box = attachedSprite->getContentSize();
		box.width *= 0.5f;
		box.height *=0.1;
		stunTimer = 1.0f;
		controller->runAnimation("BossSpawn");
		bossHealth = 1.f;
		auto physicsBody = PhysicsBody::createBox(box);
		physicsBody->setPositionOffset(Vec2(0.0f, box.height*.5f));
		physicsBody->setDynamic(true);
		physicsBody->setTag(collisionTag);
		physicsBody->setContactTestBitmask(0xFFFFFFFF);
		physicsBody->setRotationEnable(false);
		physicsBody->setLinearDamping(0.2f);
		attachedSprite->setPhysicsBody(physicsBody);
	}
	else {
		someoneBoss = false;
		//Losing boss status
		Size box = attachedSprite->getContentSize();
		box.width /= 6;
		box.height /= 20;
		auto physicsBody = PhysicsBody::createBox(box);
		physicsBody->setPositionOffset(Vec2(0.0f, box.height*.5f));
		physicsBody->setDynamic(true);
		physicsBody->setTag(collisionTag);
		physicsBody->setContactTestBitmask(0xFFFFFFFF);
		physicsBody->setRotationEnable(false);
		physicsBody->setLinearDamping(0.2f);
		attachedSprite->setPhysicsBody(physicsBody);
		stunTimer = 1.0f;
		controller->runAnimation("BossDeath");
	}
}