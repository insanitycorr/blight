#ifndef __AUDIO_PLAYER_H__
#define __AUDIO_PLAYER_H__
#include "cocos2d.h"
#include "AudioEngine.h"

USING_NS_CC;
using namespace experimental;

struct MusicHold {
public:
	std::string key;
	std::string path;
	bool loop;
	float volume;
	AudioProfile* profile = nullptr;
	MusicHold() : key(""), path(""), loop(false), volume(1.0f), profile(nullptr) {}
	MusicHold(std::string _key, std::string _path, bool _loop = false, float _volume = 1.0f, AudioProfile* _profile = nullptr) : key(_key), loop(_loop), volume(_volume), profile(_profile) {
		path = _path + "AUDIO/" + _key + "/" + _key + ".mp3";
	}
	~MusicHold() { profile = nullptr; }
};


class AudioPlayer {
public:
	AudioPlayer();
	~AudioPlayer();

	//Adds music to the list of playable tracks
	//Essentially it saves all the variables needed
	//to play the song but doesn't actually play it
	void addAudio(std::string, std::string, float=1.0f, bool = false, AudioProfile* =nullptr);

	//Creates the audio instance and plays the music
	void playAudio(std::string);

	//stops the audio instance
	void stopAudio(std::string);

	//Stops all audio instances
	void stopAllAudio();

	//Pauses the audio instance
	void pauseAudio(std::string);

	//***Different from play audio***//
	//Resumes the paused audio instance
	void resumeAudio(std::string);

	//Sets the volume of the audio
	void setVolume(std::string, float);

	//Gets the volume of the audio
	float getVolume(std::string);
	
	bool checkIfPlaying(std::string);

	//Faster versions of set volume
	//Decreases volume by 0.05f until it reaches 0.f
	void volumeDown(std::string);
	//Increases volume by 0.05f until it reaches 1.0f
	void volumeUp(std::string);

	//Fades the music out using the key and an int for the delay in frames
	//1 = every frame the music will fade a level
	//2 = every 2 frames the music will fade a level
	//so on...
	bool fadeOut(std::string, int=1);

	//Fades the music in using the key and an int for the delay in frames
	//1 = every frame the music will fade a level
	//2 = every 2 frames the music will fade a level
	//so on...
	bool fadeIn(std::string, int=1);
	

private:
	std::unordered_map<std::string, int> counterIn;
	std::unordered_map<std::string, int> counterOut;
	std::unordered_map<std::string, MusicHold> holder;
	std::unordered_map<std::string, int> audioIDs;
};
#endif;
