#pragma once

#include "cocos2d.h"
using namespace cocos2d;

class Utility
{
public:
	// Quick easy to switch color data types
	static Color4F convertColor(Color3B c);
	static Color3B convertColor(Color4F c);

	// Math Fix
	static float toRadians(float degrees);
	static float toDegrees(float radians);
};