
#include "Utility\Utility.h"

Color4F Utility::convertColor(Color3B c)
{
	return Color4F(c.r * 255.0f, c.g * 255.0f, c.b * 255.0f, 1.0f);
}

Color3B Utility::convertColor(Color4F c)
{
	return Color3B(c.r / 255.0f, c.g / 255.0f, c.b / 255.0f);
}

float Utility::toRadians(float degrees)
{
	return degrees * (3.14159f / 180.0f);
}

float Utility::toDegrees(float radians)
{
	return radians * (180.0f / 3.14159f);
}