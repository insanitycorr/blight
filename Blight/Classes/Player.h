#ifndef __PLAYER_H__
#define __PLAYER_H__
#include "cocos2d.h"
#include "InputHandler.h"
#include "Anim.h"
#include "AudioPlayer.h"

USING_NS_CC;

class Player {
public:
	Player();
	Player(Scene *s, Vec2 pos, int playerindex=0);
	~Player();
	void update(float,std::vector<Player*>,std::vector<Player*>&);
	static int getCollisionTag() { return collisionTag; }

	Sprite *getAttachedSprite() { return attachedSprite; }
	int getHealth() {
		return health;
	}
	int getBlood() {
		return blood;
	}

	void setBoom(bool t) {
		doBoom = t;
	}

	void setHealth(int i, bool relative) {
		relative ? health += i : health = i;
		if (health <= 0) {
			health = 0;

			if (doBoom) {
				auto boom = ParticleExplosion::create();
				boom->setPosition(attachedSprite->getPosition() + Vec2(0.f, 100.f));
				boom->setSpeed(512.f);
				for (int i = 0; i < 4; i++) {
					if (playerindex == i) {
						boom->setStartColor(bPath[skindex[i]]);
						break;
					}
				}
				boom->setStartColorVar(Color4F(0.f, 0.f, 0.f, 0.f));
				attachedSprite->getScene()->addChild(boom, INT_MAX - 10);
			}
			attachedSprite->removeFromParentAndCleanup(1);

		}
		if (health > 100)
			health = 100;
	}
	void setBlood(int i, bool relative) {
		relative ? blood += i : blood = i;
		if (blood < 0)
			blood = 0;
		if (blood > 100)
			blood = 100;
	}
	void setBossHealth(float i, bool relative) {
		relative ? bossHealth += i : bossHealth = i;
		if (bossHealth <= 0.f) {
			bossHealth = 0.f;
			setBoss(false);
		}
		if (bossHealth > 1.f)
			bossHealth = 1.f;
	}
	static bool someoneBoss;
	float getBossHealth() { return bossHealth; }
	static int numberOfPlayers;
	static int skindex[4];
	static std::vector<int> controlCon;
	int getIndex() { return playerindex; }
	std::string getPath() { return path; };
	int demonKills;
	int playerKills;
	float timeAlive;
	float healcd;
	void resetStats() {
		demonKills = 0;
		playerKills = 0;
		timeAlive = 0.f;
	}
	void setBoss(bool);
	bool isBoss() { return boss; }
private:
	Sprite *attachedSprite;
	static float speedcap;
	static float accelleration;
	
	AudioPlayer audioP;
	int blood,health;
	int playerindex;
	std::string path = "HERO/";
	Color4F bPath[4] { Color4F::RED, Color4F::GREEN, Color4F::YELLOW, Color4F::BLUE };
	std::string paths[4] = { "HERO/", "HERO_GREEN/", "HERO_YELLOW/", "HERO_BLUE/" };
	bool doBoom = true;
	bool moving = false;
	bool attacking = false;
	bool attackAnim = false;
	bool up = false;
	bool attackedLastFrame = false;
	bool altAttack = false;
	float attackTimer = .0f;
	float attackStampDelay = .0f;
	float stunTimer = .0f;
	bool boss;
	float bossHealth;

	Vec2 dir;

	//collisions
	static int collisionTag;

	float slowdowntime;
	Vec2 lerpFrom;
	
	Scene *sceneHandle;

	//Animation Controller
	AnimationController* controller;
};

#endif