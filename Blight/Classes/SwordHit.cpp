#include "SwordHit.h"
#include "Shape.h"

int SwordHit::collisionTag = 2;

SwordHit::SwordHit() {
	node = nullptr;
}

SwordHit::SwordHit(Scene* s, Vec2 pos, Size size, Node* source) {
	node = Node::create();
	node->setPosition(pos);
	node->setTag(SwordHit::collisionTag);
	auto phys = PhysicsBody::createBox(size);
	phys->setPositionOffset(Vec2(size.width*0.5f, size.height*.5f));
	phys->setDynamic(false);
	phys->setTag(SwordHit::collisionTag);
	phys->setContactTestBitmask(0xFFFFFFFF);
	node->setPhysicsBody(phys);
	node->getPhysicsBody()->setName(std::to_string(source->getPosition().x) + "," + std::to_string(source->getPosition().y));
	node->setName(std::to_string(source->getPosition().x) + "," + std::to_string(source->getPosition().y)+","+source->getName());

	s->addChild(node);
}

SwordHit::~SwordHit() {
	node = nullptr;
}