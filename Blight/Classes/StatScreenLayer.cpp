//#include "StatScreenLayer.h"
//#include "TestScene.h"
//#include "SimpleAudioEngine.h"
//#include "Shape.h"
//USING_NS_CC;
//
//// Print useful error message instead of segfaulting when files are not there.
//static void problemLoading(const char* filename)
//{
//    printf("Error while loading: %s\n", filename);
//    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in SplashScreenScene.cpp\n");
//}
//
//cocos2d::Scene * StatScreenLayer::createStatLayer(std::vector<Player*> players)
//{
//	auto s = StatScreenLayer::create();
//	((StatScreenLayer*)s)->players = players;
//}
//
//// on "init" you need to initialize your instance
//bool StatScreenLayer::init()
//{
//    //////////////////////////////
//    // 1. super init first
//    if ( !Scene::init() )
//    {
//        return false;
//    }
//    auto visibleSize = Director::getInstance()->getVisibleSize();
//    Vec2 origin = Director::getInstance()->getVisibleOrigin();
//
//	//Tint background
//	auto rect = new C_Rectangle(this, origin+0.5*(Vec2)visibleSize, visibleSize.width, visibleSize.height, Color4F(0.f,0.f,0.f,0.5f));
//
//    //Add Label
//	auto pauseLabel = Label::create("That's Game!", "fonts/Marker Felt.ttf", 64);
//	pauseLabel->setAnchorPoint(Vec2(0.5f, 0.5f));
//	pauseLabel->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2 + 300));
//	addChild(pauseLabel);
//
//	for (int i = 0; i < Player::numberOfPlayers; i++) {
//		{
//			auto label = Label::createWithTTF("Player "+std::to_string(i+1), "fonts/Marker Felt.ttf");
//			label->setAnchorPoint(Vec2(.5f, .5f));
//			label->setPosition(100*i+200,800);
//		}
//		{
//			auto label = Label::createWithTTF("Demon Kills: " + std::to_string(players[i]->demonKills), "fonts/Marker Felt.ttf");
//			label->setAnchorPoint(Vec2(.5f, .5f));
//			label->setPosition(100 * i + 200, 700);
//		}
//		{
//			auto label = Label::createWithTTF("Time Alive: " + std::to_string(players[i]->timeAlive), "fonts/Marker Felt.ttf");
//			label->setAnchorPoint(Vec2(.5f, .5f));
//			label->setPosition(100 * i + 200, 600);
//		}
//	}
//
//	//Add buttons
//	auto play = new MenuButton(this, "playbutton", Vec2(visibleSize.width / 2 + 500, visibleSize.height - 200));
//	play->getButton()->addTouchEventListener(CC_CALLBACK_2(StatScreenLayer::menuReplayCallback, this));
//	auto quit = new MenuButton(this, "quitbutton", Vec2(visibleSize.width / 2 - 500, visibleSize.height - 200));
//	quit->getButton()->addTouchEventListener(CC_CALLBACK_2(StatScreenLayer::menuLobbyCallback, this));
//	this->scheduleUpdate();
//
//    return true;
//}
//
//void StatScreenLayer::update(float dt) {
//
//}
//
//void StatScreenLayer::menuReplayCallback(cocos2d::Ref* pSender, ui::Widget::TouchEventType type) {
//
//}
//
//void StatScreenLayer::menuLobbyCallback(cocos2d::Ref* pSender, ui::Widget::TouchEventType type) {
//
//}
