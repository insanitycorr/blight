#include "SplashScreen.h"
#include "MenuScreen.h"
#include "TestScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene* SplashScreen::createScene()
{
    return SplashScreen::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in SplashScreenScene.cpp\n");
}

// on "init" you need to initialize your instance
bool SplashScreen::init()
{
	tt = 0.f;
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    // add "SplashScreen" splash screen"
    auto sprite = Sprite::create("MISC/LOGO.tga");
    if (sprite == nullptr)
    {
        problemLoading("'MISC/LOGO.tga'");
    }
    else
    {
		sprite->setFlippedY(true);
		sprite->setColor(Color3B::BLACK);
        // position the sprite on the center of the screen
        sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
		sprite->runAction(Sequence::create(TintTo::create(0.5f, Color3B::WHITE), DelayTime::create(1.5f), TintTo::create(2.f, Color3B::BLACK), nullptr));

        // add the sprite as a child to this layer
        this->addChild(sprite, 0);
    }

	this->scheduleUpdate();
    return true;
}

void SplashScreen::update(float dt) {

	tt += dt;
	if (tt >= 4.0f) {
		Director::getInstance()->replaceScene(MenuScreen::createScene());
	}
}


void SplashScreen::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif

    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/

    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);


}
