#ifndef __MENUSCREEN_SCENE_H__
#define __MENUSCREEN_SCENE_H__

#include "cocos2d.h"
#include "MenuButton.h"
#include "AudioPlayer.h"

class MenuScreen : public cocos2d::Scene
{
	Sprite* highlightButton;
	Sprite* play;
	Sprite* quit;
	float delay;
	//AudioPlayer audioP;
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
	void update(float dt);
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
	void menuPlayCallback(cocos2d::Ref* pSender/*, ui::Widget::TouchEventType type*/);
    // implement the "static create()" method manually
    CREATE_FUNC(MenuScreen);
};

#endif // __MENUSCREEN_SCENE_H__
