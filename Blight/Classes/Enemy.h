#ifndef __ENEMY_H__
#define __ENEMY_H__

#include "cocos2d.h"
#include "Anim.h"
#include "Player.h"
#include "Popout.h"

USING_NS_CC;

class Enemy
{
	
public:
	Enemy();
	Enemy(Scene*, Vec2);
	~Enemy();
	float stunned;
	static int getCollisionTag() { return collisionTag; }

	//Checks if enemy is dead
	bool checkIfDead();

	//Takes damage and flashes enemy red
	void takeDamage(int, Vec2=Vec2(), float=0.f);

	//Atm will basically just make the enemy run the death animation then warp back to it's starting point.
	virtual void kill();
	void cleanUpCallback(cocos2d::Ref* pSender);

	//The enemy's update function.
	virtual void update(float dt, std::vector<Player*>);

	Sprite *getAttachedSprite() { return attachedSprite; }
	Scene* sceneHandle;
protected:
	//Misc
	static int collisionTag;
	std::string path = "ENEMIES/";

	//Stats
	int health = 1;
	int attack = 5;
	int bloodOutput = 1;

	//attached sprite
	Sprite* attachedSprite;

	//Drawnode
	DrawNode *drawNode;

	//targetNode
	Node* targetNode;

	//Animation Controller
	AnimationController* controller;
};

class Ravager : public Enemy
{
	friend class AnimationController;
public:

	Ravager();
	Ravager(Scene*, Vec2, Node*);
	~Ravager();

	//Enemy-specific abilties
	void hop();
	void kill() override;
	void update(float dt, std::vector<Player*> targets) override;
private:
	bool active, left, right;
	bool attackqueue = 0;
	bool attacking = 0;
	bool chasing = 1;
	float attackTimer, attackDelayTimer, attackStampTimer;
};
class Ghoul : public Enemy
{
	friend class AnimationController;
public:

	Ghoul();
	Ghoul(Scene*, Vec2, Node*);
	~Ghoul();

	void kill() override;
	void update(float dt, std::vector<Player*> targets) override;
private:
	
	bool active, left, right;
	bool attackqueue = 0;
	bool attacking = 0;
	bool chasing = 1;
	float attackTimer, attackDelayTimer, attackStampTimer;
};


/*class Corpse {
public:
	Corpse();
	Corpse(Scene*,std::string,std::string,Vec2,float,float,int);
	~Corpse();

private:
	Sprite *attachedSprite;
	AnimationController *anim;
};*/
#endif