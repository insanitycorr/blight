#ifndef __TEST_SCENE_H__
#define __TEST_SCENE_H__

#include "cocos2d.h"
#include "Player.h"
#include "Enemy.h"
#include "InputHandler.h"
#include "DisplayHandler.h"
#include "PauseMenuLayer.h"
#include "Popout.h"
#include "AudioPlayer.h"

class TestScene : public cocos2d::Scene
{
	float pauseDelay = 0.5f;
	std::vector<Player*> playersDead;
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
	
	bool bossBarUp = false;
	bool spawnWaves = true;
	Sprite *bar_bossboi, *bar_bossboi_late, *bar_bossboi_back;
	void setBossBarOpacity(float f) {
		bar_bossboi->setOpacity(f * 255 + 10 * f);
		bar_bossboi_late->setOpacity(f * 255);
		bar_bossboi_back->setOpacity(f * 255);
	}

	bool fadeOut = true;
	bool fadeIn = true;
	bool paused;
	
	static std::vector<int> controlOrder;
	std::vector<int> controlCopy;

	Player *player, *player2, *player3, *player4;

	//audio stuffs
	static AudioPlayer audioP;
	std::vector<std::string> keys;
	int element = 0;

	//Labels
	Label *health, *blood;

	//Health Bars
	Sprite *bar_redboi, *bar_greenboi, *bar_yellowboi, *bar_blueboi;
	Sprite *bar_redboi_late, *bar_greenboi_late, *bar_yellowboi_late, *bar_blueboi_late;
	Sprite *bar_redboi_blood, *bar_greenboi_blood, *bar_yellowboi_blood, *bar_blueboi_blood;

	std::vector<Player*> players;
	std::vector<Enemy*> enemies;

	Popout *pillar1,*pillar2,*pillar3,*pillar4;
    //Callbacks
    //void menuCloseCallback(cocos2d::Ref* pSender);



	//**initializes physics callbacks
	//Like the contact begin stuffs
	void initPhysicsCallbacks();
	//**Is called when contact begins 
	bool onContactBeginCallback(PhysicsContact&);

	void update(float dt);

	//debug commands
	bool debugOn = false;
	//0 - toggle
	void pause(int _controller);
	void debugToggle();
	//***Audio Debug Commands***
	//1 - Play
	//2 - stop
	//3 - pause
	//4 - resume
	//5 - next song
	//+ - increase volume by 0.05f
	//- - decrease volume by 0.05f
	void audioDebug();
    
	// implement the "static create()" method manually
    CREATE_FUNC(TestScene);

	static Scene *sceneHandle;
	static PhysicsWorld* physicsWorld;

	float waveTimer = 10.f;
	int wave = 1;
	cocos2d::Vector<cocos2d::Node*> pausedAc;
	
};

#endif // __testscene_SCENE_H__
