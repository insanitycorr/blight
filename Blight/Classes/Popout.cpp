#include "Popout.h"

int Popout::collisionTag = 3;

Popout::Popout()
{
	attachedSprite = nullptr;
}

Popout::Popout(Scene *s, std::string image, Vec2 pos, bool animated)
{
	attachedSprite = Sprite::create(path + image);
	attachedSprite->setAnchorPoint(Vec2(.5f, 0.f));
	attachedSprite->setPosition(pos);


	Size box = Size(attachedSprite->getContentSize());
	box.height = 32;
	auto phys = PhysicsBody::createBox(box);
	phys->setTag(collisionTag);
	phys->setPositionOffset(Vec2(0.f, 16.f-attachedSprite->getContentSize().height*0.5f));
	phys->setDynamic(false);
	attachedSprite->setPhysicsBody(phys);

	s->addChild(attachedSprite);
}

Popout::~Popout()
{
	attachedSprite = nullptr;
}

void Popout::update(float dt)
{
	//The depth effect.
	attachedSprite->setZOrder(-(attachedSprite->getPosition().y));
}

