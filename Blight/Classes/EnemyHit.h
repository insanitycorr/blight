#ifndef __ENEMYHIT_H__
#define __ENEMYHIT_H__
#include "cocos2d.h"
#include "InputHandler.h"

USING_NS_CC;

class EnemyHit {
public:
	EnemyHit();
	int getDamage() { return damage; }
	Node* getNode() { return node; }
	EnemyHit(Scene *s, Vec2 pos, Size size, int damage);
	~EnemyHit();
	static int getCollisionTag() { return collisionTag; }
private:
	static int collisionTag;
	Node* node;
	int damage;
};
#endif