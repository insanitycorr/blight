#include "MenuButton.h"

MenuButton::MenuButton() {
	btn = nullptr;
}

MenuButton::MenuButton(Scene* scene, std::string image, Vec2 pos) {
	btn = ui::Button::create("UI/"+image+".png", "UI/"+image+"-press.png", "UI/" + image + "-disabled.png");
	btn->setAnchorPoint(Vec2(0.5f, 0.5f));
	btn->setPosition(pos);
	scene->addChild(btn);
}