#include "VictoryLayer.h"
#include "TestScene.h"
#include "SimpleAudioEngine.h"
#include "Shape.h"
#include "PlayerSelectScreen.h"
USING_NS_CC;

Scene* VictoryLayer::createPauseLayer(bool *pauseHook, int _controller, int playerIndex)
{
    auto scene = VictoryLayer::create();
	scene->pauseHook = pauseHook;
	scene->controller = _controller;
	scene->winner = playerIndex;
	return scene;
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in SplashScreenScene.cpp\n");
}

// on "init" you need to initialize your instance
bool VictoryLayer::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//Tint background
	auto rect = new C_Rectangle(this, origin+0.5*(Vec2)visibleSize, visibleSize.width, visibleSize.height, Color4F(0.f,0.f,0.f,0.5f));

    //Add Label
	auto pauseLabel = Label::create("That's Game!", "fonts/Marker Felt.ttf", 128);
	pauseLabel->setAnchorPoint(Vec2(0.5f, 0.5f));
	pauseLabel->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2 + 180));
	addChild(pauseLabel);

	/*auto winnerLabel = Label::create("Player " + std::to_string(winner+1) + " wins!", "fonts/Marker Felt.ttf", 48);
	winnerLabel->setAnchorPoint(Vec2(0.5f, 0.5f));
	winnerLabel->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2 + 60));
	addChild(winnerLabel);*/

	auto label = Label::create("Press A to return to character select.", "fonts/Marker Felt.ttf", 24);
	label->setAnchorPoint(Vec2(0.5f, 0.5f));
	label->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));
	addChild(label);

	this->scheduleUpdate();
    return true;
}

void VictoryLayer::update(float dt) {
	if (*pauseHook == false) {
		removeFromParentAndCleanup(true);
		return;
	}
	XINPUT->DownloadPackets(4);
	
	for (int i = 0; i < 4; i++) {
		if (XINPUT->GetConnected(i)) {
			if (XINPUT->GetButton(i, Button::A)) {
				auto tempAct = CallFuncN::create(CC_CALLBACK_1(VictoryLayer::menuBackCallback, this));
				this->runAction(tempAct);
			}
		}
	}

	//}
}

void VictoryLayer::menuResumeCallback(cocos2d::Ref* pSender/*, ui::Widget::TouchEventType type*/) {
	//Close layer off.
	Director::getInstance();
}

void VictoryLayer::menuBackCallback(cocos2d::Ref* pSender/*, ui::Widget::TouchEventType type*/) {
	//Exit application.//Close the cocos2d-x game scene and quit the application
	TestScene::controlOrder.clear();
	for (int i = 0; i < 4; i++) {
		Player::skindex[i] = i;
	}
	Player::numberOfPlayers = 0;
	Director::getInstance()->replaceScene(PlayerSelectScreen::createScene());

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif

}
