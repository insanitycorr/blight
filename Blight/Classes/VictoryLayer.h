#ifndef __VICTORYLAYER_SCENE_H__
#define __VICTORYLAYER_SCENE_H__

#include "cocos2d.h"
#include "MenuButton.h"

class VictoryLayer : public cocos2d::Scene
{
	Sprite* highlightButton;
	Sprite* play;
	Sprite* back;
	int winner;
	
public:
    static cocos2d::Scene* createPauseLayer(bool *pauseHook, int _controller, int);
	bool *pauseHook;
	int controller;
    virtual bool init();
	void update(float dt);
    // callbacks
    void menuResumeCallback(cocos2d::Ref* pSender/*, ui::Widget::TouchEventType type*/);
	void menuBackCallback(cocos2d::Ref* pSender/*, ui::Widget::TouchEventType type*/);
    // implement the "static create()" method manually
    CREATE_FUNC(VictoryLayer);
};

#endif // __PAUSEMENU_SCENE_H__
