#ifndef __SHAPE_H__
#define __SHAPE_H__

#include "cocos2d.h"
using namespace cocos2d;

#include "Utility/Utility.h"

#include <iostream>
#include <assert.h>

class C_Shape
{
protected:
	// Constructor for drawnode
	C_Shape(Scene* _parentScene, Vec2 _position = Vec2(0, 0), float _rotation = 0.f, Color4F _color = Color4F::BLACK);
	C_Shape(Node* _parentNode, Vec2 _position = Vec2(0, 0), float _rotation = 0.f, Color4F _color = Color4F::BLACK);

public:
	// No default constructor allowed
	C_Shape() = delete;
	~C_Shape();

	inline void clear()
	{
		drawNode->clear();
	}

	// Setters
	inline void setPosition(Vec2 _position)
	{
		position = _position;
		drawNode->setPosition(position);
	}
	inline void setPosition(float x, float y)
	{
		setPosition(Vec2(x, y));
	}
	inline void setRotation(float _rotation)
	{
		rotation = _rotation;
		drawNode->setRotation(rotation);
	}
	inline void setColor(Color4F _color)
	{
		color = _color;
		// Shape needs to be redrawn to change color
		clear();
		drawNode->setColor(Utility::convertColor(color));
		createShape();
	}

	// Increasers
	inline void increasePosition(Vec2 amount)
	{
		setPosition(position + amount);
	}
	inline void increaseRotation(float amount)
	{
		setRotation(rotation + amount);
	}

	// Getters
	inline Vec2		 getPosition()	const
	{
		return position;
	}
	inline float	 getRotation()	const
	{
		return rotation;
	}
	inline Color4F	 getColor()		const
	{
		return color;
	}
	inline DrawNode* getDrawNode()	const
	{
		return drawNode;
	}

protected:
	// Parent Scene
	Scene * parentScene = nullptr;

	// DrawNode
	DrawNode* drawNode = nullptr;

	// Shape Data
	Vec2	position;
	Vec2	anchorOffset;
	float	rotation;
	Color4F color;

	// Redraw Function
	virtual void createShape() {}

};
class C_Rectangle : public C_Shape
{
public:
	C_Rectangle(Scene* _parentScene, Vec2 _position = Vec2(0, 0), float _width = 50.0f, float _height = 50.0f, Color4F _color = Color4F::BLACK)
		: C_Shape(_parentScene, _position, 0.f, _color)
	{
		width = _width;
		height = _height;

		createShape();
	}
	C_Rectangle(Node* _parentNode, Vec2 _position = Vec2(0, 0), float _width = 50.0f, float _height = 50.0f, Color4F _color = Color4F::BLACK)
		: C_Shape(_parentNode, _position, 0.f, _color)
	{
		width = _width;
		height = _height;

		createShape();
	}

	inline float getLeftX()
	{
		return position.x - width * 0.5f;
	}
	inline float getRightX()
	{
		return position.x + width * 0.5f;
	}
	inline float getTopY()
	{
		return position.y + height * 0.5f;
	}
	inline float getBottomY()
	{
		return position.y - height * 0.5f;
	}

private:
	// Rectangle Only Data
	float width;
	float height;

	// Redraw
	void createShape() override
	{
		drawNode->drawSolidRect(Vec2(-width * 0.5f, -height * 0.5f), Vec2(width*0.5f, height*0.5f), color);
	}
};

#endif