#include "Anim.h"

AnimationController::AnimationController() {
	refSprite = nullptr;
}

AnimationController::AnimationController(Sprite* sp)
{
	refSprite = sp;
}

AnimationController::~AnimationController()
{
	animations.clear();
	refSprite = nullptr;
}

void AnimationController::runAnimation(std::string key, Action* special)
{
	refSprite->stopAllActions();
	if (special == nullptr) {
		refSprite->runAction(Animate::create(animations.at(key)));
	}
	else {
		refSprite->runAction(Sequence::create(Animate::create(animations.at(key)), special, NULL));
	}
}
void AnimationController::runAnimationIfReady(std::string key, Action* special)
{
	if (special == nullptr) {
		refSprite->runAction(Animate::create(animations.at(key)));
	}
	else {
		refSprite->runAction(Sequence::create(Animate::create(animations.at(key)), special, NULL));
	}
}

void AnimationController::addAnimation(std::string key, std::string _path, int loops, float durFrame) {
	anim = new Animation();
	anim = Animation::create();
	int counter = 0;

	//AUTOMATIC ANIMATION CREATION STATION
	do {
		std::string path = _path + "ANIMATIONS/" + key + "/" + key + std::to_string(counter + 1) + ".png";

		//If sprite doesn't exist (because picture doesn't) end the loop
		auto sprite = Sprite::create(path);
		if (sprite == nullptr) { 
			break; 
		}

		//if sprite does exist, add the picture to the animation
		anim->addSpriteFrameWithFile(path); 
		counter++;
	} while (true);
	anim->setLoops(loops); 
	anim->setDelayPerUnit(durFrame); //duration per anim frame
	anim->setRestoreOriginalFrame(true);
	animations.insert(std::make_pair(key, anim));
	animations[key]->retain();
}