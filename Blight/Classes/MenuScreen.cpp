#include "MenuScreen.h"
#include "PlayerSelectScreen.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene* MenuScreen::createScene()
{
    return MenuScreen::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in SplashScreenScene.cpp\n");
}

// on "init" you need to initialize your instance
bool MenuScreen::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	// add background
	auto background = Sprite::create("UI/TitleBackground2.png");
	background->setAnchorPoint(Vec2(.0f, .0f));
	background->setPosition(origin);
	this->addChild(background);

    // add play button
	/*play = new MenuButton(this, "playbutton", Vec2(300, visibleSize.height / 2 - 100));
	
	quit = new MenuButton(this, "quitbutton", Vec2(300, visibleSize.height / 2 - 220));*/

	play = Sprite::create("UI/playbutton.png");
	play->setPosition(Vec2(300, visibleSize.height / 2 - 100));

	highlightButton = Sprite::create("UI/bloodsplat.png");
	highlightButton->setPosition(Vec2(165, 50));
	highlightButton->retain();
	play->addChild(highlightButton, 1, "selec");
	this->addChild(play, 0);

	quit = Sprite::create("UI/quitbutton.png");
	quit->setPosition(Vec2(300, visibleSize.height / 2 - 220));
	//quit->addChild(highlightButton, 1, "selec");
	this->addChild(quit);

	TestScene::audioP.addAudio("background1", "ENVIRONMENT/", 0.3f, 1);
	if (!TestScene::audioP.checkIfPlaying("background1")) {
		TestScene::audioP.playAudio("background1");
	}
	this->scheduleUpdate();
    return true;
}

void MenuScreen::update(float dt) {
	
	XINPUT->DownloadPackets(4);
	delay += dt;
	if (delay <= 0.5) {
		XINPUT->DownloadPackets(4);
	}
	else {
		for (int i = 0; i < 4; i++) {
			if (XINPUT->GetConnected(i)) {
				if (XINPUT->GetButton(i, Button::DPadDown)) {
					if (quit->getChildByName("selec") == nullptr) {
						play->removeChildByName("selec");
						/*highlightButton = Sprite::create("UI/bloodsplat.png");
						highlightButton->setPosition(Vec2(165, 50));*/
						quit->addChild(highlightButton, 1, "selec");
					}
					break;
				}
				if (XINPUT->GetButton(i, Button::DPadUp)) {
					if (play->getChildByName("selec") == nullptr) {
						quit->removeChildByName("selec");
						/*highlightButton = Sprite::create("UI/bloodsplat.png");
						highlightButton->setPosition(Vec2(165, 50));*/
						play->addChild(highlightButton, 1, "selec");
					}
					break;
				}
				if (XINPUT->GetButton(i, Button::A)) {
					if (play->getChildByName("selec") == nullptr) {
						//makes sure that quit isn't a nullptr either
						if (quit->getChildByName("selec") != nullptr) {
							quit->runAction(CallFuncN::create(CC_CALLBACK_1(MenuScreen::menuCloseCallback, this)));
						}
						break;
					}
					else {
						play->runAction(CallFuncN::create(CC_CALLBACK_1(MenuScreen::menuPlayCallback, this)));
					}
				}
			}
		}
	}
}

void MenuScreen::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif

    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/

    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);


}

void MenuScreen::menuPlayCallback(cocos2d::Ref * pSender/*, ui::Widget::TouchEventType type*/)
{
	//if (type==ui::Widget::TouchEventType::ENDED)
		Director::getInstance()->replaceScene(PlayerSelectScreen::createScene());
}
